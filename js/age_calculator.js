/* Today's Date Initial */
var today = new Date();
var currentMonth = today.getMonth();
var currentDate = today.getDate();
var currentYear = today.getFullYear();
var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var slideValue;
var dobYear;
var userDateselection = new Date();


var ageWidget = {};
ageWidget.create = function(obj, opt) {
    $(obj).each(function() {
        var currentSlider = $(this).find(".age-calc-slider");
        etGeneric.valueSlider.init(currentSlider, opt)
    })
}
ageWidget.skeleton = {

    monthTable: function(c_month, c_year, c_obj, callback) {
        var selectedDate = new Date(c_year, c_month, 1);
        $(c_obj).find(".ageWidget-cont .monthTable").remove();

        $(c_obj).find(".ageWidget-cont").append("<div class='monthTable'><label class='cal-title'>select the <span>month</span></label></div>")
        var tbl = $("<table/>").addClass("table")
        var tblLbl = $("<div/>").addClass("tbl-label")
        var tblCont = $("<div/>").addClass("tbl-cont col-sm-12")
        tblCont.append(tblLbl).append(tbl).append(tbl);

        var tblContWrap = $("<div/>").addClass("tbl-cont-wrap col-sm-24")
        tblContWrap.append(tblCont);
        $(c_obj).find(".ageWidget-cont .monthTable").append(tblContWrap);
        var domEle = $(c_obj).find(".ageWidget-cont .monthTable .tbl-cont-wrap").html()
        $(c_obj).find(".ageWidget-cont .monthTable .tbl-cont-wrap").append(domEle);
        for (var i = 0; i < 4; i++) {
            $(c_obj).find(".ageWidget-cont .monthTable table").eq(0).append("<tr>")
            $(c_obj).find(".ageWidget-cont .monthTable table").eq(1).append("<tr>")
            for (var j = 0; j < 3; j++) {
                $(c_obj).find(".ageWidget-cont .monthTable table").eq(0).find("tr:last-child").append("<td>&nbsp;</td>")
                $(c_obj).find(".ageWidget-cont .monthTable table").eq(1).find("tr:last-child").append("<td>&nbsp;</td>")
            }
        }
        var cnt = 0;
        var monthoffsetVal = selectedDate.getMonth() + 1
        for (var i = 0; i < 12; i++) {

            if (c_year == selectedDate.getFullYear()) {
                $(c_obj).find(".ageWidget-cont .monthTable .tbl-cont").eq(0).find(".tbl-label").html(selectedDate.getFullYear())
                $(c_obj).find(".ageWidget-cont .monthTable table").eq(0).find("td").eq(i).attr('data-month', selectedDate.getMonth()).attr('data-year', selectedDate.getFullYear()).html("<span>" + monthNames[selectedDate.getMonth()] + "</span>")
                selectedDate.setMonth(selectedDate.getMonth() + 1);
            }
        }
        for (var i = 0; i < monthoffsetVal; i++) {
            if (c_year = selectedDate.getFullYear()) {
                $(c_obj).find(".ageWidget-cont .monthTable .tbl-cont").eq(1).find(".tbl-label").html(selectedDate.getFullYear())
                $(c_obj).find(".ageWidget-cont .monthTable table").eq(1).find("td").eq(i).attr('data-month', selectedDate.getMonth()).attr('data-year', selectedDate.getFullYear()).html("<span>" + monthNames[selectedDate.getMonth()] + "</span>")
                selectedDate.setMonth(selectedDate.getMonth() + 1);
            }
        }


        ageWidget.skeleton.monthSelection(c_obj, callback);

    },
    monthSelection: function(c_obj, callback) {
        $(c_obj).find(".ageWidget-cont #datetimepicker").remove();

        $(c_obj).find(".ageWidget-cont .monthTable td").on('click', function() {
            $(c_obj).find(".ageWidget-cont .monthTable td").removeClass("active");
            $(this).addClass("active");
            var datepicker_Month = $(this).attr('data-month');
            var datepicker_Year = $(this).attr('data-year');
            var customDate = new Date(datepicker_Year, parseInt(datepicker_Month) + 1, 0)
            var lastDateOfTheMonth = customDate.getDate();
            console.log(datepicker_Month + datepicker_Year)
            $("#datetimepicker").remove();

            $(c_obj).find(".ageWidget-cont").append("<div id='datetimepicker' class='age-dp'></div>")
            if ($(window).width() < 768) {
                $(c_obj).find(".ageWidget-cont .monthTable").hide();
            }

            $(c_obj).find('#datetimepicker .cal-title').remove()
            $("#datetimepicker").append("<label class='cal-title'>select the <span>date</span></label>")
            $(c_obj).find('#datetimepicker').datetimepicker({
                inline: true,
                useCurrent: false,
                collapse: false,
                defaultDate: false,
                format: 'DD',
                viewMode: "days"
            });

            var cDatePicker = $(c_obj).find('#datetimepicker').data("DateTimePicker");






            var tableIndx =  $(".tbl-cont-wrap .tbl-cont").index($(this).parents(".tbl-cont"))
            //console.log(tableIndx)

            if (datepicker_Month == currentMonth && tableIndx == "0") {
                console.log("month" + currentMonth + "date" + currentDate);
                console.log("month" + currentMonth + "date" + currentDate);

                cDatePicker.date(new Date(datepicker_Year, datepicker_Month, currentDate+1));
                cDatePicker.minDate(new Date(datepicker_Year, datepicker_Month, currentDate+1));
                cDatePicker.maxDate(new Date(datepicker_Year, datepicker_Month, lastDateOfTheMonth));
            } else if (datepicker_Month == currentMonth && tableIndx == "1"){

                console.log("insde next")

                cDatePicker.date(new Date(datepicker_Year, datepicker_Month, 01));
                cDatePicker.minDate(new Date(datepicker_Year, datepicker_Month, 01));
                cDatePicker.maxDate(new Date(datepicker_Year, datepicker_Month, currentDate));
            } else {

                cDatePicker.date(new Date(datepicker_Year, datepicker_Month, 01));
                cDatePicker.minDate(new Date(datepicker_Year, datepicker_Month, 01));
                cDatePicker.maxDate(new Date(datepicker_Year, datepicker_Month, lastDateOfTheMonth));
            }



            $(c_obj).find('#datetimepicker .day.active').on('click', function() { //22-3-16

                var curEle = $(this).attr("data-day").split("/")
        
                $(c_obj).find('#datetimepicker .cal-title').remove()
                $(c_obj).find(".ageWidget-cont .monthTable").remove();
                $(c_obj).find(".ageWidget-cont").hide();
                cDatePicker.hide();

               $(".date-show").html(curEle[1] + "-" + (monthNames[datepicker_Month]) + "-" + datepicker_Year);
                
                if (callback != undefined) { //30-3-16
                    callback();
                }
            });

            

            $(c_obj).find('#datetimepicker').on('dp.change', function(ev) {
         
                var userDateselection = new Date(ev.date);
                $(".date-show").html(userDateselection.getDate() + "-" + (monthNames[userDateselection.getMonth()]) + "-" + userDateselection.getFullYear())

                if (callback != undefined) {
                    callback();
                }

                //$(c_obj).find('#datetimepicker').data("DateTimePicker").hide();
                $(c_obj).find('#datetimepicker .cal-title').remove()
                $(c_obj).find(".ageWidget-cont .monthTable").remove();
                $(c_obj).find(".ageWidget-cont").hide();
                cDatePicker.hide();

            });




        });
    }
}

$(function() {
    var opt = {
        start: [18],
        range: {

            'min': 18,
            'max': 70
        },
        step: 1,
        tooltips: wNumb({
            decimals: 0
        }),
        format: wNumb({
            decimals: 0,

        }),
        evupdate: function(obj) {
            //console.log("update")
            slideValue = obj.noUiSlider.get();
            $(".ageWidget-cont").show();
            dobYear = currentYear - (slideValue);
            ageWidget.skeleton.monthTable(currentMonth, dobYear - 1, $(obj).parent());
        }
    }
    ageWidget.create(".age-cal", opt);
    /* For Refrence */
    // $(".age-cal").each(function(){
    //     ageWidget.create(this, opt);
    // })


});