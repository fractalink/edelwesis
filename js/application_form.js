/*
  @Author : Fractalink Design Studio
  @Project : Edelwesis Website
  @Dev : Irfan Khan
  @Date : 1st Feb 2016;

*/


function formSteps(obj) {
    $(obj).each(function(e) {
        var currObj = this;

        if ($(window).width() < 768) {
            var containerW = $(currObj).find(".steps").eq(0).width();

            $(currObj).find(".steps").width(containerW);
        } else {
            var containerW = 980;
        }

        var itemCount = $(currObj).find(".steps").length;
        var totalWidth = parseInt(containerW) * parseInt(itemCount);

        //shows 1st  step 
        var stID = $(currObj).find(".steps").eq(0).attr("id");

        $(currObj).find(".step-view-mode").find("#" + stID).animate({ left: (0) }, 600, function() {
            var h = $("#" + stID).height();
            $(".step-view-mode").height(h);
            $("#" + stID).addClass("active")
        });

        $(currObj).find(".prev-btn").on('click', function() {
            var idVal = $(this).attr("href");

            $(currObj).find(".step-view-mode .steps").not(".active").css({ left: "-980px" })
            $(currObj).find(".step-view-mode .active").animate({ left: "980px" }, 600)
            $(currObj).find(".step-view-mode").find(idVal).animate({ left: 0 }, 600, function() {
                var h = $(currObj).find(idVal).height();
                $(currObj).find(".steps").removeClass("active");
                $(currObj).find(idVal).addClass("active");
                $(currObj).find(".step-view-mode").height(h)
                console.log("prev123")
            });
            return false;
        });


        $(currObj).find(".next-btn").on('click', function() {
            var idVal = $(this).attr("href");
            var btnIndx = $(this).index(".step-view-mode-wrap .steps .next-btn");

            $(currObj).find(".step-view-mode .steps").not(".active").css({ left: "980px" })
            $(currObj).find(".step-view-mode .active").animate({ left: '-'+slideWidth }, 600)
            $(currObj).find(".step-view-mode").find(idVal).animate({ left: 0 }, 600, function() {
                var h = $(currObj).find(idVal).height();
                $(currObj).find(".steps").removeClass("active");
                $(currObj).find(idVal).addClass("active");
                $(currObj).find(".step-view-mode").height(h)
                console.log("prev123")
            });
            return false;

        });
        $(currObj).find(".nav-btn").on('click', function() {
            var idVal = $(this).attr("href");

            $(currObj).find(".step-view-mode .steps").not(".active").css({ left: "980px" })
            $(currObj).find(".step-view-mode .active").animate({ left: "-980px" }, 600)
            $(currObj).find(".step-view-mode").find(idVal).animate({ left: 0 }, 600, function() {
                var h = $(currObj).find(idVal).height();
                $(currObj).find(".steps").removeClass("active");
                $(currObj).find(idVal).addClass("active");
                $(currObj).find(".step-view-mode").height(h)
            });
            return false;

        });
    });




}


/* Document Ready */
$(function() {

    if ($(".step-view-mode-wrap").length != 0) {
        formSteps(".step-view-mode-wrap");
    }

    $(window).resize(function() {
        if ($(".step-view-mode-wrap").length != 0) {
            formSteps(".step-view-mode-wrap");
        }
    });

});
