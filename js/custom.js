/*
  @Author : Fractalink Design Studio
  @Project : Edelwesis Website
  @Dev : Irfan Khan
  @Date : 26th Nov 2015;

*/

var ET = {};

ET.golbalVar = {
    animationTime: 800,
    easing: "ease",
    quietPeriod: 200, // 21-3-16
    totalSlide: 0,
    lastAnimation: 0
}

ET.buildLayout = {
    init: function() {
        $(".main-wrapper .section-wrapper").each(function(e) {
            var topVal = (e * 100 + "%");
            $(this).css('top', topVal);
            var toolTipText = $(this).attr("data-tooltip")

            if (e == '0') {
                $("body").append("<div class='side-nav'><ul><li class='active'><a href='javascript:void(0)'><span class='home-nav-tooltip'><span class='text'>"+toolTipText+"</span></span></a></li></ul></div>");
            } else {
                $(".side-nav ul").append("<li><a href='javascript:void(0)'><span class='home-nav-tooltip'><span class='text'>"+toolTipText+"</span></span></a></li></li>");
            }
        });

        ET.golbalVar.totalSlide = $(".main-wrapper .section-wrapper").length;
        $(".main-wrapper .section-wrapper").eq(0).addClass("active");


        $(".side-nav li").on('click', function() {
            var indx = $(".side-nav li").index(this);
            ET.buildLayout.clickActive(indx);
        });

    },
    getCurrentSlide: function() {
        var ind = $(".main-wrapper .section-wrapper").index($(".section-wrapper.active"));
        return ind;
    },
    animatePanel: function(pos) {

        if ($("html").hasClass("ie") == true) {

            $(".main-wrapper").each(function(e) {
                $(this).animate({
                    "top": pos + "%"
                }, ET.golbalVar.animationTime)
            })



        } else {
            $(".main-wrapper").css({
                "-webkit-transform": "translate3d(0, " + pos + "%, 0)",
                "-webkit-transition": "all " + ET.golbalVar.animationTime + "ms " + ET.golbalVar.easing,
                "-moz-transform": "translate3d(0, " + pos + "%, 0)",
                "-moz-transition": "all " + ET.golbalVar.animationTime + "ms " + ET.golbalVar.easing,
                "-ms-transform": "translate3d(0, " + pos + "%, 0)",
                "-ms-transition": "all " + ET.golbalVar.animationTime + "ms " + ET.golbalVar.easing,
                "transform": "translate3d(0, " + pos + "%, 0)",
                "transition": "all " + ET.golbalVar.animationTime + "ms " + ET.golbalVar.easing
            });
        }



    },
    moveUp: function(indxObj) {
        //console.log("index" + indxObj);
        if (indxObj == null) {

            var currentVal = (ET.buildLayout.getCurrentSlide() > 0) ? (ET.buildLayout.getCurrentSlide() - 1) : ET.buildLayout.getCurrentSlide()

        } else {
            var currentVal = indxObj;

        }

        if (currentVal > 0) {
            $(".header-wrapper").css('background', '#fff');
            $(".dock-wrap").addClass("active");
        } else if (currentVal == 0) {
            $(".header-wrapper").css('background', 'rgba(255, 255, 255, 0.65) none repeat scroll 0 0');
            $(".dock-wrap").removeClass("active");
        }

        var pos = -((currentVal) * 100);
        ET.buildLayout.animatePanel(pos, 'upEase');
        $(".section-wrapper").removeClass("active")
        $(".section-wrapper").eq(currentVal).addClass("active")
        $(".side-nav li").removeClass("active");
        $(".side-nav li").eq(currentVal).addClass("active")
    },
    moveDown: function(indxObj) {
        if (indxObj == null) {
            var currentVal = (ET.buildLayout.getCurrentSlide() < (ET.golbalVar.totalSlide - 1)) ? (ET.buildLayout.getCurrentSlide() + 1) : ET.buildLayout.getCurrentSlide()
        } else {
            var currentVal = indxObj;
        }

        if (currentVal > 0) {
            $(".header-wrapper").css('background', '#fff');
            $(".dock-wrap").addClass("active");

        } else if (currentVal == 0) {
            $(".header-wrapper").css('background', 'rgba(255, 255, 255, 0.65) none repeat scroll 0 0');
            $(".dock-wrap").removeClass("active");
        }


        var pos = -((currentVal) * 100);
        ET.buildLayout.animatePanel(pos, 'downEase')
        $(".section-wrapper").removeClass("active")
        $(".section-wrapper").eq(currentVal).addClass("active")
        $(".side-nav li").removeClass("active");
        $(".side-nav li").eq(currentVal).addClass("active")

    },
    clickActive: function(indxObj) {
        if (indxObj > ET.buildLayout.getCurrentSlide()) {
            ET.buildLayout.moveDown(indxObj);
        } else if (indxObj < ET.buildLayout.getCurrentSlide()) {
            ET.buildLayout.moveUp(indxObj);
        }

    },
    keyAnimation: function() {
        $(window).keydown(function(event) {

            if($(".hs-home-wrap").hasClass("active") == false)
            {


            if (event.which == 40) {

                ET.buildLayout.moveDown();
            }
            if (event.which == 38) {
                ET.buildLayout.moveUp()
            }
            }
        });
    }

}

function scrollEvent() {


    $(".home .main-wrapper").mousewheel(function(event) {

        var timeNow = new Date().getTime();
        if (timeNow - ET.golbalVar.lastAnimation < ET.golbalVar.quietPeriod + ET.golbalVar.animationTime) {
            event.preventDefault();
            return;
        }

        ET.golbalVar.lastAnimation = timeNow

        if (event.deltaY > 0) {
            ET.buildLayout.moveUp()
        } else if (event.deltaY < 0) {
            ET.buildLayout.moveDown();
        }
    });



}

/* Document Ready */
$(function() {

    $("a").each(function() {
        if ($(this).attr('href') == '#' || $(this).attr('href') == ' ') {
            $(this).attr('href', 'javascript:void(0)');
        }
    });


    if ($(window).width() < 768) {
        $('.home').removeClass("home-scroll").addClass('no-scroll');

    } else {
        $('.home').addClass("home-scroll").removeClass('no-scroll');
        ET.buildLayout.init();
        ET.buildLayout.keyAnimation();
        scrollEvent();
        $(".footer-title").removeClass("toggle-footer");
    }








});
