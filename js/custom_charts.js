/*
  @Author : Fractalink Design Studio
  @Project : Edelwesis Website
  @Dev : Irfan Khan
  @Date : 29th Jan 2015;
*/

var customCharts = {};
var chrtObj = {}
var totalChartCntr = 0;
customCharts.buildLayout = {
    bar: function(bar_init, bar_data, bar_options, bar_responsiveOptions) {;

        var barchart = new Chartist.Bar(bar_init, bar_data, bar_options, bar_responsiveOptions);

        var seq = 0;

        $(bar_init).attr("data-chart-name", "chart_" + totalChartCntr)
            //var chObj = {}
        chrtObj["chart_" + totalChartCntr] = barchart;
        //chrtObj.push(chObj)
        totalChartCntr++
        barchart.on('created', function() {
            seq = 0;
        });

        barchart.on('draw', function(data) {
            if (data.type === 'bar') {
                data.element.animate({

                    y2: {
                        begin: seq++ * 80,
                        dur: 500,
                        from: data.y1,
                        to: data.y2,
                        // You can specify an easing function name or use easing functions from Chartist.Svg.Easing directly
                        easing: Chartist.Svg.Easing.easeOutQuart
                    }
                });
            }
        });



    },
    pie: function(pie_init, pie_data, pie_options) {
        var piechart = new Chartist.Pie(pie_init, pie_data, pie_options);
        $(pie_init).attr("data-chart-name", "chart_" + totalChartCntr)
            //var chObj = {}
        chrtObj["chart_" + totalChartCntr] = piechart;
        //chrtObj.push(chObj)
        totalChartCntr++

    }
}

function createGraph(obj) {
    if ($(".fund_bargraph").length != 0) {

        var data = {
            labels: ['1 year', '3 year', 'since inception'],
            series: [{
                "name": "Fund Performance",
                "data": [12, 13, 15]

            }, {

                "name": "Benchmark",

                "data": [11, 12, 13]
            }]
        };

        var options = {
            seriesBarDistance: -30,
            plugins: [
                Chartist.plugins.legend({
                    legendNames: ['Benchmark', 'Fund Performance']
                }),
                Chartist.plugins.tooltip(),
                Chartist.plugins.ctAxisTitle({
                    axisY: {
                        axisTitle: 'Amount Invested',
                        axisClass: 'ct-axis-title',
                        textAnchor: 'middle',
                        offset: {
                            x: 0,
                            y: 20
                        },
                        flipTitle: true
                    }
                })
            ],
            axisX: {
                showGrid: false
            }
        };

        var responsiveOptions = [
            ['screen and (min-width: 780px)', {
                seriesBarDistance: -30,
                chartPadding: {
                    top: 10,
                    right: 0,
                    bottom: 0,
                    left: 40
                },
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value;
                    }
                }
            }],
            ['screen and (max-width: 768px)', {
                seriesBarDistance: 20,
                chartPadding: {
                    top: 10,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
                axisX: {
                    labelInterpolationFnc: function(value) {
                        return value[0];
                    }
                }
            }]
        ];


        customCharts.buildLayout.bar(obj, data, options, responsiveOptions);

    }
}

function createDemoPortfolioGraph(obj) {


    var data = {
        labels: ['bond fund', 'money market fund', 'equity large cap fund', 'equity top 250 fund', 'managed fund', 'price earning based fund'],
        series: [{
            "name": "Fund Performance",
            "data": [12, 13, 15, 30, 28, 15]

        }, {

            "name": "Benchmark",

            "data": [11, 12, 13, 15, 30, 28]
        }]
    };

    var options = {
        seriesBarDistance: -30,
        plugins: [
            Chartist.plugins.legend({
                legendNames: ['Benchmark', 'Fund Performance']
            }),
            Chartist.plugins.tooltip(),
            Chartist.plugins.ctAxisTitle({
                axisY: {
                    axisTitle: 'Amount Invested',
                    axisClass: 'ct-axis-title',
                    textAnchor: 'middle',
                    offset: {
                        x: 0,
                        y: 20
                    },
                    flipTitle: true
                }
            })
        ],
        axisX: {
            showGrid: false
        }
    };

    var responsiveOptions = [
        ['screen and (min-width: 780px)', {
            seriesBarDistance: -30,
            chartPadding: {
                top: 10,
                right: 0,
                bottom: 0,
                left: 40
            },
            axisX: {
                labelInterpolationFnc: function(value) {
                    return value;
                }
            }
        }],
        ['screen and (max-width: 768px)', {
            seriesBarDistance: -10,
            chartPadding: {
                top: 10,
                right: 0,
                bottom: 0,
                left: 0
            },
            axisX: {
                labelInterpolationFnc: function(value) {
                    return value[0];
                }
            }
        }],
        ['screen and (max-width: 400px)', {
            seriesBarDistance: -10,
            chartPadding: {
                top: 10,
                right: 0,
                bottom: 0,
                left: 0
            },
            axisX: {
                labelInterpolationFnc: function(value) {
                    return value[0];
                }
            }
        }]
    ];


    customCharts.buildLayout.bar(obj, data, options, responsiveOptions);
}

function createPiGraph(obj) {
    if ($(".fund_piegraph").length != 0) {

        var data = {
            series: [4, 6]
        };
        var sum = function(a, b) {
            return a + b
        };

        var arr = ['Equity', 'Debt'];

        var opt = {
            labelInterpolationFnc: function(value) {
                return Math.round(value / data.series.reduce(sum) * 100) + '%';
            }
        }


        customCharts.buildLayout.pie(obj, data, opt);


    }
}
/* Document Ready */
$(function() {

    var chtCont = $(".fund_bargraph");
    for (var i = 0; i < chtCont.length; i++) {
        createGraph(chtCont[i]);
    }

    


    var piChtCont = $(".fund_piegraph");
    for (var i = 0; i < piChtCont.length; i++) {
        createPiGraph(piChtCont[i])
    }


    if ($(".fund_bargraph_portfolio").length != 0) {
        var fund_bargraph_portfolio = $(".fund_bargraph_portfolio")
        createDemoPortfolioGraph(fund_bargraph_portfolio[0])
    }





});
