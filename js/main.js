/*
  @Author : Fractalink Design Studio
  @Project : Edelwesis Website
  @Dev : Irfan Khan
  @Date : 26th Nov 2015;

*/
var etGeneric = {};


etGeneric.slideshow = {
    init: function(obj, option) {
        $(obj).owlCarousel(option);

        $(".slider-nav li").eq("0").addClass("active");

        $(".slider-nav li").eq(0).addClass("active")

        $(obj).on('translated.owl.carousel', function(event) {
            var indx = $(".owl-dots .owl-dot.active").index();
            $(".slider-nav li").removeClass("active");
            $(".slider-nav li").eq(indx).addClass("active");

        });


    },
    clickEvent: function(obj) {
        $('.slider-nav li .icon').click(function() {
            var indx = $(this).index(".slider-nav li .icon");
            $(obj).trigger('to.owl.carousel', [indx, "800"])
        });
    }
}

etGeneric.roundCarousel = {
    init: function(radiusVal) {
        radiusVal = (radiusVal != undefined) ? radiusVal : 200
        $('#roundCarousel').Cloud9Carousel({
            yOrigin: 30,
            yRadius: -40,
            xRadius: radiusVal,
            buttonLeft: $(".product-list-wrap  .prev"),
            buttonRight: $(".product-list-wrap .next"),
            autoPlay: 0,
            bringToFront: true,
            itemClass: "list",
            onRendered: rendered,
            onLoaded: function() {
                // showcase.css( 'visibility', 'visible' )
                // showcase.css( 'display', 'none' )
                // showcase.fadeIn( 1500 )
            }
        });

        function rendered(carousel) {
            //title.text( carousel.nearestItem().element.alt )
            console.log(carousel.floatIndex());
            $('#roundCarousel').find(".list").removeClass("active")
            $('#roundCarousel').find(".product-item").removeClass("orange")
            $('#roundCarousel').find(".list").eq(carousel.nearestIndex()).addClass("active")
            $('#roundCarousel').find(".list").eq(carousel.nearestIndex()).find(".product-item").addClass("orange")
                // Fade in based on proximity of the item
                //var c = Math.cos((carousel.floatIndex() % 1) * 2 * Math.PI)
                //title.css('opacity', 0.5 + (0.5 * c))
        }
    }
}

etGeneric.footerToggle = {
    init: function() {
        $(".toggle-footer").on('click', function() {
            $(this).toggleClass("active");
            $(".footer-wrap").toggleClass("active");

            if ($(this).hasClass("active") == true) {
                console.log($(".footer-wrap").position().top)
                console.log($(".footer-wrap").offset().top);
                setTimeout(function() {
                    $(".main-wrapper").animate({
                        scrollTop: $(".main-wrapper").scrollTop() + ($(".footer-wrap").position().top -150)
                    }, 800);

                }, 300);

            } else {
                $(".footer-wrap").scrollTop(0);
            }

        });
    }
}

etGeneric.customTabs = {
    init: function(obj) {

        $(obj).each(function() {
            var currentTab = this;
            var tabLink = $(currentTab).children(".tab-link-wrap").find(".tab-link");
            var tabCont = $(currentTab).children(".tab-content-wrap").children(".tab-content");
            $(tabLink).removeClass("active");
            $(tabCont).hide();

            var mobileTest = etGeneric.windowSize.mobile();

            if (mobileTest == true) {

                if ($(currentTab).hasClass(".overlay-tabs") != true) {
                    $(tabCont[0]).show();
                    $(tabLink[0]).addClass("active");
                }
            } else {
                $(tabCont[0]).show();
                $(tabLink[0]).addClass("active");
            }


            $(tabLink).click(function() {
                $(".overlay-tabs").children(".tab-content-wrap").addClass("open");

                $(tabLink).removeClass("active");
                $(this).addClass("active");
                var src = $(tabLink).index(this);
                $(tabCont).hide();
                $(tabCont).eq(src).stop().fadeIn(200, function() {
                      if ($(this).find(".social-connect-listing").length != 0) {

                        $(".social-connect-listing").masonry();
                        //etGeneric.customMasonry.init(".social-connect-listing", opt);

                    }

                    

                    if ($(".product-list.equalizer-height").length != 0) {
                        etGeneric.equalizerHeight.init(".product-list.equalizer-height", ".equalizer-item");

                    }




                    //console.log( $(".tab-with-line .tab-link").length)
                    if ($(".fund_bargraph").length != 0) {
                        var indx = $(".tab-with-line .tab-link.active").index();
                        //console.log("ind" + indx)
                        //console.log($(".tab-content").eq(indx).find(".fund_bargraph").attr("data-chart-name"))
                        var ch = chrtObj[$(".tab-content").eq(indx).find(".fund_bargraph").attr("data-chart-name")]
                        ch.update();
                    }



                });

                return false;
            });

            $(".section-back").on('click', function() {

                $(".tabs").children(".tab-content-wrap").removeClass("open");

            });
        })

    }
}

etGeneric.customAccordian = {

    init: function(obj, objHead, objCont, defaltIndx, defOpn) {
        //.log(objCont)
        $(obj).each(function() {

            var accHead = $(this).find(objHead);
            var accCont = $(this).find(objCont);

            $(accCont).hide();
            if (defOpn == 'open') {
                var dInd = (defaltIndx != undefined) ? defaltIndx : 0;
                $(accHead).eq(dInd).addClass("open");
                $(accCont).eq(dInd).show(1);
            }

            var ctr = []
            $(accCont).each(function() {
                var opt = {
                    aply: 0
                }
                ctr.push(opt)
            })

            if (objCont == ".buzz-cont") {
                $(objHead).eq(defaltIndx).parents(".buzz-item").addClass("active")
            } else {
                $(objHead).eq(defaltIndx).parent().addClass("active")
            }



            $(objHead).click(function() {

                if (!$(this).hasClass("open") == true) {
                    if (objCont == ".buzz-cont") {
                        $(objHead).parent().parent().removeClass("active")
                        $(this).parent().parent().addClass("active")
                    } else {

                        $(objHead).parent().removeClass("active")
                        $(this).parent().addClass("active")
                    }

                } else if ($(this).hasClass("open") == true) {
                    if (objCont == ".buzz-cont") {

                        $(this).parent().parent().removeClass("active");
                    } else {
                        $(this).parent().removeClass("active");
                    }

                }
            })


            $(accHead).click(function() {
                var indx = $(accHead).index(this)
                if ($(this).hasClass("open")) {

                    $(accCont).eq(indx).slideUp(200, function() {

                    });
                    $(this).removeClass("open");
                } else {
                    $(accHead).removeClass("open");
                    $(this).addClass("open");

                    $(accCont).each(function(e) {

                        if (e === indx) {

                            $(accCont).eq(e).slideDown(400, function() {

                                if (ctr[e].aply == 0) {


                                }
                                ctr[e].aply = 1

                            });

                        } else {
                            $(accCont).eq(e).slideUp(400);
                        }
                    })
                }

            });
        })
    }
}


etGeneric.teamDescShow = {
    init: function() {
        var teamDescAttr = {
            class: "team-profile-desc col-sm-12"
        }

        //alert($(".team-list .profile").eq(0).find("img").attr('src'))
        var profileH = "";
        $(".team-list .profile img").eq(0).one("load", function() {
            profileH = $(".team-list .profile").height() - 1;
        }).each(function() {
            if (this.complete) $(this).load();
        });



        $(".team-intro-wrap").height(profileH);
        var mobileTest = etGeneric.windowSize.mobile();

        if (mobileTest == false) {
            $(".team-profile-desc").hide()
        }

        var teamDescWrap = $("<div/>").attr(teamDescAttr);
        $(".team-list").after(teamDescWrap);

        $(".team-list .profile").on('click', function() {
            var indx = $(".team-list .profile").index(this);
            // console.log(indx);
            $(".team-list .profile").removeClass("active");
            $(".team-list .profile").eq(indx).addClass("active");
            var dt = $(".team-list .team-profile-cont").eq(indx).html();

            $(".team-profile-desc").addClass("active");


            if (mobileTest == false) {
                etGeneric.teamDescShow.layout(indx);
                $(".team-profile-desc").html(dt).show(1);
            } else {
                $(".team-profile-desc").html(dt);
            }

        });

        $(".team-profile-desc ").on('click', '.close-icon', function() {
            if (mobileTest == false) {
                $(this).parent(".team-profile-desc").hide(1);
            }
            $(".team-list .profile").removeClass("active");
            $(".team-profile-desc").removeClass("active");
        });

    },

    layout: function(indx) {

        var profileH = "";
        $(".team-list .profile img").eq(0).one("load", function() {
            profileH = $(".team-list .profile").height() - 1;
        }).each(function() {
            if (this.complete) $(this).load();
        });



        $(".team-intro-wrap").height(profileH);
        var pos = ((indx + 1) / 4).toString();
        var heightVal = profileH;
        var splitVal = pos.split(".");
        console.log(pos)

        $(".team-profile-desc").removeClass("left-arrow").removeClass("right-arrow");
        if (splitVal[1] == undefined || splitVal[1] == "0") {
            splitVal[1] = "25%"
            $(".team-profile-desc").addClass("left-arrow");

        } else if (splitVal[1] == "25") {
            splitVal[1] = "50%"
            $(".team-profile-desc").addClass("left-arrow");

        } else if (splitVal[1] == "75") {
            splitVal[1] = "25%"
            $(".team-profile-desc").addClass("right-arrow");

        } else if (splitVal[1] == "5") {
            splitVal[1] = "0%"
            $(".team-profile-desc").addClass("right-arrow");

        }



        var totalLen = $(".team-list .profile").length;
        var modDivide = Math.ceil(totalLen / 4) - 1;


        var topPos = (splitVal[0] < modDivide) ? splitVal[0] : (modDivide - 1);
        var vClass = (splitVal[0] < modDivide) ? "" : "bot-arr";

        console.log("topPos: " + topPos + " split: " + splitVal[0] + " modVal: " + modDivide + "len:" + totalLen)

        $(".team-profile-desc").css({
            top: topPos * profileH,
            left: splitVal[1],
            height: (profileH * 2)
        }).removeClass("bot-arr").addClass(vClass);


        //alert(indx);

        // for(var i=startIndex;i<totalLen;i++)
        // {
        //   console.log(i);



        //   $(".team-profile-desc").css({
        //     top: modDivide * profileH,
        //     left: splitVal[1],
        //     height: (profileH * 2)
        // });
        // }

    }
}


etGeneric.searchModule = {
    init: function() {
        $(".search-box").on('click', function() {
            etGeneric.screenOverlay.closeStatus();
            $(".screen-overlay").addClass("search-overlay");
            setTimeout(function() {
                $(".global-search").addClass("active");
            }, 200);
        });


    }

}

etGeneric.screenOverlay = {
    initVar: {
        scrollPos: ""
    },
    init: function() {
        $(".screen-overlay").on('click', function() {

            etGeneric.screenOverlay.closeStatus();

        });
    },
    closeStatus: function() {

        var mobileTest = etGeneric.windowSize.mobile();
        $(".main-action-level").removeClass("active");

        $(".screen-overlay").attr('class', 'screen-overlay');
        $(".support-wrap,.reveal-cont,.reveal-link,.get-quote-wrap,.need-calculator,.tool-tip-sendMe-overlay,.intelligence-msg,.hs-home-wrap").removeClass("active");


        if (mobileTest) {
            $(".footer-title,.header-wrapper").removeClass('posHide')
        }
    },
    scrollBlock: function() {
        etGeneric.screenOverlay.initVar.scrollPos = $(window).scrollTop();
        $("body").addClass("blocked-scroll").css('top', (-etGeneric.screenOverlay.initVar.scrollPos));
    },
    scrollActive: function() {
        // var scrTop = ($(window).scrollTop() > 0)? $(window).scrollTop() : 0;
        // etGeneric.screenOverlay.initVar.scrollPos = scrTop;
        $("body").removeClass("blocked-scroll");
        $(window).scrollTop(etGeneric.screenOverlay.initVar.scrollPos);
    }

}


etGeneric.circularCarouselTab = {
    init: function(carouselObj) {

        $(carouselObj).each(function() {
            var cObj = this;
            var sliderTab = $(cObj).find(".circular-carousel-tab");
            var sliderCont = $(cObj).find(".circular-carousel-cont");
            var centerIndx = $(sliderTab).find("ul li").index($(sliderTab).find("ul li.center"));
            var slideTabLen = $(sliderTab).find("ul li").length;
            var displayItem = 3;
            var cloneItem = (displayItem - 1);
            var diffCloneItem = cloneItem - centerIndx;

            $(sliderCont).find("ul li.nav-cont-list").removeClass("center").eq(centerIndx).addClass("center")


            var activeContHtml = $(sliderTab).find("ul li").eq(centerIndx).find(".nav-link-cont").html();
            $(cObj).find(".active-tab").html(activeContHtml).find("a").removeAttr("href");


            $(sliderTab).find("ul li").each(function(e) {
                $(this).attr("itm_no", e)
            })
            $(sliderCont).find("ul li.nav-cont-list").each(function(e) {
                $(this).attr("cont_no", e)
            })

            $(sliderTab).find("ul").width($(sliderTab).find("ul li").width() * $(sliderTab).find("ul li").length)
            $(sliderCont).find("> ul").width($(sliderCont).find("> ul li.nav-cont-list").width() * $(sliderCont).find("> ul li.nav-cont-list").length)

            var initPos = function() {
                var lPos = (($(sliderTab).find("ul li").width()) * (cloneItem / 2))
                $(sliderTab).find("ul").css({
                    left: -lPos
                })

            }
            initPos();
            var contLpos = $(sliderCont).find("ul li.nav-cont-list").width() * centerIndx

            $(sliderCont).find("ul li.nav-cont-list").hide();
            $(sliderCont).find("ul li.nav-cont-list").eq(centerIndx).show();

            var changePos = function(indx) {
                var cenIndx = $(sliderTab).find("ul li").index($(sliderTab).find("ul li.center"));
                //console.log(indx + " : " + cenIndx)
                diffIndx = Math.abs(indx - cenIndx);

                var aniPos = $(sliderTab).find("ul li").width() * indx - (($(sliderTab).find("ul li").width()) * (cloneItem / 2))
                var contIndx = $(sliderTab).find("ul li").eq(indx).attr("itm_no")
                $(sliderCont).find("ul li.nav-cont-list").fadeOut(500);
                $(sliderCont).find("ul li.nav-cont-list").eq(contIndx).fadeIn(500);
                // if (indx > cenIndx) {

                $(sliderTab).find("ul").animate({
                    left: -aniPos
                }, 300, function() {

                    if (indx > cenIndx) {
                        for (var i = 0; i < diffIndx; i++) {
                            var obj = $(sliderTab).find("ul li").eq(0)

                            $(sliderTab).find("ul").append(obj);
                            var lPos = $(sliderTab).find("ul").position().left + $(sliderTab).find("ul li").width();
                            //console.log(lPos + " : " + $(sliderTab).find("ul").css("left"))
                            $(sliderTab).find("ul").css("left", lPos)
                        }


                    }
                    if (indx < cenIndx) {
                        for (var i = 0; i < diffIndx; i++) {
                            var obj = $(sliderTab).find("ul li").eq($(sliderTab).find("ul li").length - 1)
                            $(sliderTab).find("ul").prepend(obj);
                            var lPos = $(sliderTab).find("ul").position().left - $(sliderTab).find("ul li").width();
                            //console.log(lPos + " : " + $(sliderTab).find("ul").css("left"))
                            $(sliderTab).find("ul").css("left", lPos)


                        }
                    }

                });

                $(sliderTab).find("ul li").removeClass("center").eq(indx).addClass("center");
                var activeContHtml = $(sliderTab).find("ul li").eq(indx).find(".nav-link-cont").html();
                $(cObj).find(".active-tab").html(activeContHtml).find("a").removeAttr("href");




            }

            $(cObj).find(".btn-next").on("click", function() {
                var ind = $(sliderTab).find("ul li").index($(sliderTab).find("ul li.center"));
                changePos(ind + 1);
            })
            $(cObj).find(".btn-prev").on("click", function() {
                var ind = $(sliderTab).find("ul li").index($(sliderTab).find("ul li.center"));
                changePos(ind - 1)
                return false;
            })
            $(sliderTab).find("ul").on("click", "li", function() {
                var indx = $(sliderTab).find("ul li").index(this);
                changePos(indx)
            })


        })

    }
}


etGeneric.customDateTimepicker = {
    cal_Date: function(dateObj) {
        $(dateObj).each(function() {
            $(this).find("input").datetimepicker({
                format: 'MM/DD/YYYY',
                useCurrent:true,
                viewMode:'years',
                defaultDate: '1/1/1976'
            });
        })


    },
    cal_frm_to: function(compObj) {
        $(compObj).each(function() {
            var frmObj = $(this).find(".fromInput");
            var toObj = $(this).find(".toInput");
            $(frmObj).find("input").datetimepicker({
                format: 'MM/DD/YYYY',
                viewMode:'years',
                defaultDate: '1/1/1976'
            });
            $(toObj).find("input").datetimepicker({
                format: 'MM/DD/YYYY',
                viewMode:'years',
                defaultDate: '1/1/1976'
            });
            $(frmObj).find("input").on("dp.change", function(e) {
                $(toObj).find("input").data("DateTimePicker").minDate(e.date);
            });
            $(toObj).find("input").on("dp.change", function(e) {
                $(frmObj).find("input").data("DateTimePicker").maxDate(e.date);
            });

        })
    },
    cal_Time: function(timeObj) {

        $(timeObj).datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            forceParse: 0,
            minView: 0,
            maxView: 2,
            showMeridian: 1


        });

    },
    cal_Yead: function(yearObj) {

        $(yearObj).datetimepicker({
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 4,
            forceParse: 0,
            minView: 4,
            maxView: 4

        });

    }
}


etGeneric.customMasonry = {
    init: function(obj, option) {
        $(obj).masonry(option);
    }

}

etGeneric.showHide = {
    init: function(parentEle, showHide, actionEle) {
        $(actionEle).on('click', function() {
            $(this).parents(parentEle).toggleClass("active");
            $(this).parents(parentEle).find(showHide).slideToggle('00');
        });
    }
}


etGeneric.tabArrow = {
    init: function() {
        $(".basic-tabs,.lang-tabs").children(".tab-link-wrap").find(".tab-link").each(function(e) {

            $(this).append("<span class='arrow-icon'></span>");
            var borderWidth = Math.floor($(this).width() / 2);

            $(this).find(".arrow-icon").css({
                "border-left-width": borderWidth + "px",
                "border-right-width": borderWidth + "px"
            })

        });

    }
}

etGeneric.menuInteraction = {
    init: function() {
        var mobileTest = etGeneric.windowSize.mobile();
        if (!mobileTest) {

            etGeneric.menuInteraction.createSkeleton();
        }

        //doc menu click handler
        $(".menu-btn").on('click', function() {

            if ($(this).hasClass("active") == true) {
                etGeneric.screenOverlay.closeStatus();
                $(this).removeClass("active");
                $(".menu-item").removeClass("active");
                $(".menu-list").removeClass("active");
                $(".screen-overlay").removeClass("main-mnu-ovrlay");

            } else {
                etGeneric.screenOverlay.closeStatus();
                $(this).addClass("active");
                $(".menu-list").addClass("active");
                $(".screen-overlay").addClass("main-mnu-ovrlay")
            }

        });

        if (mobileTest) {
            $(".menu-item").on("click", function() {
                $(".menu-item").removeClass("active");
                $(this).addClass("active");
                if ($(this).find(".flyout-menu-wrapper").length > 0) {
                    var dtitle = $(this).find(".menu-action-item").text()
                    var data = $(this).find(".flyout-menu-wrapper").html()
                    $(".mobile-flyout").find(".flyout-title a").text(dtitle)
                    $(".mobile-flyout").find(".flyout-cont").html(data)
                    $(".mobile-flyout").addClass("active");

                }

            })
            $(".flyout-title a").on("click", function() {
                $(this).parents(".mobile-flyout").removeClass("active")
                $(".menu-item").removeClass("active")
            })
        } else {
            $(".menu-item").on("click", function() {
                $(".flyout-item-container").mCustomScrollbar("scrollTo", "left", {
                    scrollInertia: 0
                });

                $(".menu-item").removeClass("active");
                $(this).addClass("active");

                $(this).find(".flyout-item").removeClass("active").eq(0).addClass("active")
            })

            $(".flyout-item").hover(function() {

                $(".flyout-item-container").mCustomScrollbar("scrollTo", "left", {
                    scrollInertia: 0
                });

                $(this).parent().find(".flyout-item").removeClass("active");
                $(this).addClass("active");
            })
        }

    },
    createSkeleton: function() {
        var dSetting = {
            class: "flyout-sub-item-wrapper",
            style: function() {
                var w = $(this).find(".flyout-sub-item-col").eq(0).width() * $(this).find(".flyout-sub-item-col").length
                return "width:" + w + "px;";
            }

        }
        $(".flyout-menu-wrapper").each(function() {
            var flyMnuWrap = $(this)

            flyMnuWrap.find(".flyout-sub-item-wrapper").each(function() {
                var cWrap = $(this);
                var subEleArr = cWrap.find("a")
                var colEle
                var parentCol = $("<div />").addClass("p_col")
                    //console.log("length : "+subEleArr.length)
                subEleArr.each(function(e) {
                    if ((e % 12) == 0) {
                        //console.log("dome")
                        colEle = $("<div />").addClass("flyout-sub-item-col");
                        colEle.append($(this))
                        parentCol.append(colEle)
                    } else {
                        //console.log("not dome")
                        colEle.append($(this))
                    }
                })
                var scrWrapper = $("<div />").addClass("flyout-item-container")
                var ele = $("<div />").append(parentCol.html());

                cWrap.wrap(scrWrapper).after(ele);
                ele.attr(dSetting);
                cWrap.remove();
            })
        })
    }

}


etGeneric.supportInteraction = {
    init: function() {

        if ($(".hs-home-wrap ").hasClass("active") == true) {

            setTimeout(function() {
                $(".mascot-popup.intial").removeClass("intial");
                //console.log("first step")
                setTimeout(function() {
                    $(".mascot-popup").addClass("intial");
                    //console.log("inside step")
                    etGeneric.supportInteraction.msgAnimation();
                }, 5000)

            }, 500);

        }

        var mobileTest = etGeneric.windowSize.mobile();

        $(".character,.mascot-popup,.support-back").on('click', function() {

            if ($(".support-wrap").hasClass('active') == true) {

                etGeneric.screenOverlay.closeStatus();

                $(".support-wrap").removeClass("active");
                if (mobileTest) {

                    $(".footer-title,.header-wrapper").removeClass('posHide')
                }
            } else {
                etGeneric.screenOverlay.closeStatus();
                //etGeneric.screenOverlay.scrollBlock();



                $(".screen-overlay").addClass("support-overlay");
                $(".support-wrap").addClass("active");
                if (mobileTest) {
                    $(".footer-title,.header-wrapper").addClass('posHide')
                }
            }





        });

        $(".support-wrap .reveal-link,.support-footer-list li").on('click', function() {

            $(".screen-overlay").addClass("support-overlay");
            $(".support-wrap").addClass("active");

            var dataRel = $(this).attr("rel");
            //alert(dataRel)

            $(".reveal-link").removeClass("active");

            if ($(this).hasClass("mascot-popup") == true) {
                $(".reveal-link[rel='chat']").addClass("active");
            } else {
                $(".reveal-link[rel=" + dataRel + "]").addClass("active");
            }

            $(".support-wrap .reveal-cont").each(function() {
                var matchRel = $(this).attr("cont-rel");

                if (matchRel == dataRel) {

                    $(".reveal-cont").removeClass("active");

                    setTimeout(function() {
                        $(".reveal-cont[cont-rel=" + dataRel + "]").addClass("active");
                    }, 200);

                }

            });

        });

        $(".whatsapp-link").on('click', function() {
            $(".whatsapp-wrap").slideToggle();
        });
    },
    msgAnimation: function() {
        setTimeout(function() {
            $(".mascot-popup.intial").removeClass("intial");
            setTimeout(function() {
                $(".mascot-popup").addClass("intial");
                etGeneric.supportInteraction.msgAnimation();
            }, 10000)

        }, 25000);
    }
}

etGeneric.customeSelect = {
    init: function(obj, opt) {
        $(obj).select2(opt)
    }
}

etGeneric.helperAppNav = {
    init: function() {
        $('.popup-wrap').on("click", function() {

            $(".screen-overlay").addClass("helper-nav-overlay");
            if ($(this).hasClass("free-quote-wrap") == true) {

                if ($(this).parents().hasClass("section-wrapper")) {
                    var l = $(this).offset().left
                } else {
                    var l = "50%";
                    $(".get-quote-wrap .overlay-box").css("margin-left", '-150px');
                }

                $(".get-quote-wrap").addClass("active");
                $(".get-quote-wrap .overlay-box").css("left", l)
                $(".get-quote-wrap .overlay-box .accor").removeClass("active");
                $(".get-quote-wrap .overlay-box .personal-details-wrap").addClass("active");
                $(".get-quote-wrap .result-wrap .edit-btn").removeClass("opn")
                $(".get-quote-wrap .result-wrap .edit-btn").text("edit");
                $(".get-quote-wrap .result-wrap .tool-tip-selectRate").hide();
                $(".tool-tip-sendMe-overlay").removeClass("active");
                $eventSelect.select2("val", "");
            }
            if ($(this).hasClass("calculator-wrap") == true) {
                if ($(this).parents().hasClass("section-wrapper")) {
                    var l = $(this).offset().left
                } else {
                    var l = "50%";
                    $(".need-calculator .overlay-box").css("margin-left", '-150px');
                }
                $(".need-calculator").addClass("active");
                $(".need-calculator .overlay-box").css("left", l)
            }
        })

        var $eventSelect = $(".builder-select-box");
        var cCont = "";
        $eventSelect.select2("val", "");
        $eventSelect.on("change", function(e) {
            //console.log(e);
            //console.log(e.currentTarget);
            cCont = e.currentTarget.value;
            $(".get-quote-wrap .msg").removeClass("active")
            $(".get-quote-wrap .msg-"+cCont).addClass("active")
            // if (cCont != "") {
            //     $(".get-quote-wrap .overlay-box .accor").removeClass("active");
            //     $("." + cCont).addClass("active");
            // }

        });
        $(".get-quot-btn").on("click",function(){
            var cCont = $eventSelect.select2("val")
            $(".get-quote-wrap .overlay-box .accor").removeClass("active");
            $("." + cCont).addClass("active");
        })
        $(".get-quote-wrap .back-link").on("click", function() {
            if ($(this).hasClass("per-detail") == true) {
                $(".get-quote-wrap .overlay-box .accor").removeClass("active");
                $(".personal-details-wrap").addClass("active");
                $eventSelect.select2("val", "");
            } else if ($(this).hasClass("get-quote") == true) {
                $(".get-quote-wrap").removeClass("active");
                $(".screen-overlay ").removeClass("helper-nav-overlay")
            } else {
                $(".get-quote-wrap .overlay-box .accor").removeClass("active");
                $("." + cCont).addClass("active");
            }


        })

        $(".need-calculator .back-link").on("click", function() {
            if ($(this).hasClass("need-calc") == true) {
                $(".need-calculator").removeClass("active");
                $(".screen-overlay ").removeClass("helper-nav-overlay")
            }
        })


        $(".request-call-back").on("click", function() {

            $(".get-quote-wrap .overlay-box .accor").removeClass("active");
            $(".request-to-call-wrap").addClass("active");

        })
        $(".send-detail").on("click", function() {
            $(".get-quote-wrap .tool-tip-sendMe-overlay").addClass("active")

        })
        $(".get-quote-wrap .tool-tip-sendMe-overlay").on("click", function(e) {

            if ($(e.target).hasClass("tool-tip-sendMe") == false && $(e.target).hasClass("tool-tip-sendMe-overlay") == true) {
                $(this).removeClass("active");

            }

        })
        $(".get-quote-wrap .result-wrap .edit-btn").on("click", function() {
            if ($(this).hasClass("opn") == false) {
                $(".get-quote-wrap .result-wrap .tool-tip-selectRate").show();
                $(".get-quote-wrap .result-wrap .edit-btn").text("close");
                $(this).addClass("opn")
            } else {

                $(this).removeClass("opn")
                $(".get-quote-wrap .result-wrap .edit-btn").text("edit");
                $(".get-quote-wrap .result-wrap .tool-tip-selectRate").hide();
            }

        })
        $(".get-quote-wrap .result-wrap .tool-tip-selectRate .select-rate").on("click", function() {
            $(".get-quote-wrap .result-wrap .tool-tip-selectRate .select-rate").removeClass("active");
            $(this).addClass("active");
            $(".get-quote-wrap .result-wrap .info-amount-val").text($(this).text());
            $(".get-quote-wrap .result-wrap .tool-tip-selectRate").hide();
            $(".get-quote-wrap .result-wrap .edit-btn").text("edit");
            $(".get-quote-wrap .result-wrap .edit-btn").removeClass("opn");

        })

    }
}

etGeneric.windowSize = {
    mobile: function() {
        if ($(window).width() < 768) {
            return true;

        } else {
            return false;

        }
    }
}

etGeneric.mobile_FooterToogle = {
    init: function() {
        var mobileTest = etGeneric.windowSize.mobile();

        if (mobileTest == true) {
            $(".quick-item-cont h3").on('click', function() {
                $(this).parent().toggleClass("active");
                $(this).parent().find(".quick-list").slideToggle(300);

            });
        }
    }
}

etGeneric.equalizerHeight = {
    init: function(obj, objlist, groupCount) {
        var eqObj = $(obj);

        $(eqObj).find(objlist).each(function() {
            $(this).css('height', '');
        })
        console.log($(eqObj).find(objlist).height());
        // var groupof = 3;
        var groupof = (groupCount != undefined) ? groupCount : $(eqObj).find(objlist).length;
        var dtColl = [];
        var maxHeight = 0;
        var gropObj = {};
        gropObj.eleVal = [];

        $(eqObj).find(objlist).each(function(e) {
            if ((e + 1) % groupof == 0) {
                gropObj.eleVal.push($(this).height())
                if (gropObj.eleVal.length != 0) {
                    dtColl.push(gropObj);
                    console.log(dtColl);
                }
                gropObj = {};
                gropObj.eleVal = [];

            } else {
                gropObj.eleVal.push($(this).height())
            }

        });
        var cnt = 0;
        //alert(cnt)
        $(eqObj).find(objlist).each(function(e) {
            var h = (Math.max.apply(Math, dtColl[cnt].eleVal))
                //console.log(h)
            $(this).css("height", h)
            if ((e + 1) % groupof == 0) {
                cnt++
            }

        })

    }
}

etGeneric.customScrollbar = {
    init: function(obj, opt) {
        $(obj).mCustomScrollbar(opt);

    }
}

etGeneric.valueSlider = {
    init: function(s_obj, s_opt) {
        var handlesSlider = $(s_obj);
        //  console.log(s_opt);
        handlesSlider.each(function(e) {
            cSlObj = this;
            noUiSlider.create(cSlObj, s_opt);
            cSlObj.noUiSlider.on('change', function() {
                if (s_opt.evupdate != undefined)
                    s_opt.evupdate(cSlObj);

                //slideValue = cSlObj.noUiSlider.get();
                slideValue = cSlObj
                    .noUiSlider
                    .get();
            });
            cSlObj.noUiSlider.on('slide', function() {
                if (s_opt.evslide != undefined)
                    s_opt.evslide(cSlObj);
                slideValue = cSlObj.noUiSlider.get();
                // console.log(slideValue)
            });
        })
    }
}


/* For Refrence */
// etGeneric.valueSlider = {
//     init: function(s_obj, s_opt) {
//         var handlesSlider = $(s_obj);
//         //handlesSlider.each(function(e) {
//         //handlesSlider = this;
//         noUiSlider.create(handlesSlider[0], s_opt);
//         handlesSlider[0].noUiSlider.on('change', function() {
//             if (s_opt.evupdate != undefined)
//                 s_opt.evupdate(handlesSlider[0]);

//             //slideValue = handlesSlider[0].noUiSlider.get();
//         });
//         handlesSlider[0].noUiSlider.on('slide', function() {
//             if (s_opt.evslide != undefined)
//                 s_opt.evslide(handlesSlider[0]);
//             //slideValue = cSlObj.noUiSlider.get();
//             //console.log(cSlObj)
//         });
//         //})
//     }
// }



etGeneric.quickView = {
    layout: function(obj, arrangeOpt, colClass, objClass) {
        var mobileTest = etGeneric.windowSize.mobile();

        if (!mobileTest) {
            $(obj).each(function(e) {
                var currObj = this;
                var objClass = $(currObj).attr('class');
                var listCount = arrangeOpt.length;
                $(currObj).after("<ul class='arranged-section row " + objClass + "'></ul>");

                for (var i = 0; i < arrangeOpt.length; i++) {
                    $(currObj).parent().find(".arranged-section").append("<li class=" + colClass + "></li>");
                    for (var j = 0; j < arrangeOpt[i].length; j++) {
                        var currEle = $(currObj).find(".arrange-item").eq(arrangeOpt[i][j]).clone();
                        console.log(currEle);
                        $(currObj).parent().find(".arranged-section > li").eq(i).append(currEle)
                    }
                }

                $(currObj).remove();
            });
        }
        //$(obj).remove()
    }
}

etGeneric.addnewStructBefore = {
    init: function(clickBtn, targetEle) {
        var obj = $(targetEle).clone();
        $(clickBtn).on('click', function() {
            $(obj).clone().show().insertBefore(clickBtn);
            $(".dynamic-select-box").select2("destroy");
            if ($(".dynamic-select-box").length != 0) {
                var opt = {
                    "dropdownAutoWidth": true,
                    "minimumResultsForSearch": -1,
                    "containerCssClass": "select-box",
                    "dropdownCssClass": "select-box-list",
                    "dropdownCss": "test1"
                }

                etGeneric.customeSelect.init(".dynamic-select-box", opt);

                // Your Profile Page   - Profile Image Edit Click Tooltipster
                $(".editTooltip").tooltipster({
                    functionInit: function() {
                        return $(this).parent().find(".editTooltip-content").html()
                    },
                    theme: 'theme1',
                    position: 'bottom-left',
                    trigger: 'click',
                    contentAsHTML: true,
                    arrow: false,
                    positionTracker: true,
                    onlyOne: true
                })
            }
            if ($(".dynamic-datePicker").length != 0) {
                etGeneric.customDateTimepicker.cal_Date(".dynamic-datePicker");
            }
            // add remove class js : your profile
            if ((".your-profile-wrap input[type=text]").length != 0) {
                etGeneric.addRemoveClassFunc.init();
            }
        })
    }
}

etGeneric.trackAppContenToggle = {
    init: function() {
        $(".track-application-wrap .step-process-list").on('click', function() {
            var indx = $(this).index()

            if ($(this).hasClass("completed") || $(this).hasClass("initiated")) {
                var contIndx = indx + 1;
                $(".content").hide();
                $(".content" + contIndx).show();
                var clsName = "contentSelected" + contIndx;
                $(".content-show-wrap").removeClass($(this).attr('class').slice(0, -1));
                $(".content-show-wrap").addClass(clsName);
            }

        })
    }
}

etGeneric.accountsubNavToggle = {
    init: function(toggleEle, targetEle) {
        $(toggleEle).on("select2-selecting", function(e) {
            window.location.href = $(targetEle).find(".sub-list-sec li").eq(e.val).find("a").attr('href')
        })
    }
}

etGeneric.intelligenceMsg = {
    init: function() {
        $(".msg-btn").on('click', function() {
            etGeneric.screenOverlay.closeStatus();
            $(".screen-overlay").addClass("intelligence-msg-overlay");
            setTimeout(function() {
                $(".intelligence-msg").addClass("active");
            }, 200);
        });
    }
}



etGeneric.helpTutorialPosFunc = {
    init: function(arryObj) {
        var windowW = $(window).width();
        var len = arryObj.length;
        var iconH = $(".helper-app-nav-wrap .helper-app-nav .icon").height();
        $(".screen-overlay").addClass("help-screen-overlay")
        $(".hs-home-wrap").addClass("active");
        if(windowW < 768)
        {
            for(var i=0;i<=len-1;i++)
            {
                if(i==0)
                {
                    
                     var insideLen = arryObj[i].length;
                     for(var j=0;j<=insideLen-1;j++)
                     {
                        var p = $(arryObj[i][j]);
                        var offset = p.offset();
                        var array = [".hs-home-2",".hs-home-3",".hs-home-4"];
                        var minusHeight = $(array[j]).height();

                        var h = minusHeight - iconH;
                        console.log(minusHeight+" : "+iconH)
                        $(array[j]).css({
                            top: offset.top - h + "px",
                            left:offset.left+ "px"
                        })
                        var mobArray = [".hs-home-2"];
                        $(mobArray[j]).css({
                            top: offset.top + "px",
                            left:offset.left+ "px"
                        })
                     }

                }
                else if(i == 1)
                {
                    var insideLen = arryObj[i].length;
                     for(var j=0;j<=insideLen-1;j++)
                     {
                        var p = $(arryObj[i][j]);
                        var offset = p.offset();
                        var array = [".hs-home-5"];
                        $(array[j]).css({
                            top: offset.top + 55 + "px",
                            left:offset.left - 137 + "px"
                        })
                     }
                }
                else if(i==2)
                {
                    var insideLen = arryObj[i].length;
                     for(var j=0;j<=insideLen-1;j++)
                     {
                        var p = $(arryObj[i][j]);
                        var offset = p.offset();
                        var array = [".hs-home-6"];
                        var minuswidth = $(array[j]).width();
                        var iconH = $(arryObj[i][j]).height();
                        var h = minusHeight - iconH;
                        $(array[j]).css({
                            top: offset.top - h + "px",
                            right:0
                        })
                     }
                }
               
            }
        }
        else if(windowW > 768)
        {
            for(var i=0;i<=len-1;i++)
            {
                if(i==0)
                {
                    
                     var insideLen = arryObj[i].length;
                     for(var j=0;j<=insideLen-1;j++)
                     {
                        var p = $(arryObj[i][j]);
                        var offset = p.offset();
                        var array = [".hs-home-2",".hs-home-3",".hs-home-4"];
                        var minusHeight = $(array[j]).height();
                        var h = minusHeight - iconH;
                        $(array[j]).css({
                            top: offset.top - h + "px",
                            left:offset.left - 83+ "px"
                        })
                     }

                }
                else if(i == 1)
                {
                    var insideLen = arryObj[i].length;
                     for(var j=0;j<=insideLen-1;j++)
                     {
                        var p = $(arryObj[i][j]);
                        var offset = p.offset();
                        var array = [".hs-home-5"];
                        $(array[j]).css({
                            top: offset.top + 77  + "px",
                            left:offset.left - 221 + "px"
                        })
                     }
                }
                else if(i==2)
                {
                    var insideLen = arryObj[i].length;
                     for(var j=0;j<=insideLen-1;j++)
                     {
                        var p = $(arryObj[i][j]);
                        var offset = p.offset();
                        var array = [".hs-home-6"];
                        var minusHeight = $(array[j]).height();
                        var minuswidth = $(array[j]).width();
                        var iconH = $(arryObj[i][j]).height();
                        var h = minusHeight - iconH;
                        $(array[j]).css({
                            top: offset.top - h + "px",
                            right:0
                        })
                     }
                }
               
            }
        }
            
        etGeneric.helpScreenFunc.init();
 }}

etGeneric.helpScreenFunc = {
    init: function() {
        $(".help-nav-listing ul li").removeClass("active");
        $(".help-nav-listing ul li").eq(0).addClass("active");
        $(".hs-home").removeClass("active");
        $(".hs-home").eq(0).addClass("active");
        $(".help-nav-listing ul li").on('click', function() {
            var obj = $(this).attr("data-sequence")
            var indx = $(this).index();
            $(".help-nav-listing ul li").removeClass("active");
            $(".help-nav-listing ul li").eq(indx).addClass("active")
            $(".hs-home").each(function() {
                $(".hs-home").removeClass("active")
                $(".hs-home").eq(indx).addClass("active")
            })
        })
        $(".hs-home .btn").on("click", function() {
            var parntIndx = $(this).parents(".hs-home").index();
            $(".hs-home").removeClass("active");
            $(".hs-home").eq(parntIndx).addClass("active");
            $(".help-nav-listing ul li").removeClass("active");
            $(".help-nav-listing ul li").eq(parntIndx).addClass("active");
        })

        $(".hs-home-6 .btn,.linking-wrap .btn").on('click', function() { // 21-3-16
            etGeneric.screenOverlay.closeStatus();
            $(".banner-wrap .owl-carousel").trigger('play.owl.autoplay',[1000]);
            setTimeout(function() {
                $(".mascot-popup.intial").removeClass("intial");
                console.log("first step")
                setTimeout(function() {
                    etGeneric.supportInteraction.msgAnimation();
                }, 5000)

            }, 500);

            $(".hs-home-wrap").removeClass('active');
        })
    }
}


// check uncheck js : need analyzer
etGeneric.chckUnchckFunc = {
    init: function(btn, targetEle, yesClsNam, noClsNam) {
        $(targetEle).addClass("active")
        $(btn).on('change', function() {
            if ($(this).parent().hasClass(yesClsNam) == true) {
                $(targetEle).addClass("active")
            } else if ($(this).parent().hasClass(noClsNam) == true) {
                $(targetEle).removeClass("active")
            }
        })
    }
}


// add remove class js : your profile
etGeneric.addRemoveClassFunc = {
    init: function() {
        $('.myaccount-wrap.your-profile-wrap input[type=text]').each(function() {
            var text_value = $(this).val();
            if (text_value != '') {
                $(this).parents(".form-item ").addClass("active")
            }
        })

        $(".myaccount-wrap.your-profile-wrap .form-item input[type=text]").on('click', function() {
            $('.myaccount-wrap.your-profile-wrap input[type=text]').each(function() {
                var text_value = $(this).val();
                if (text_value != '') {
                    $(this).parents(".form-item ").addClass("active")
                } else if (text_value == '') {
                    $(this).parents(".form-item ").removeClass("active")
                }
            })
            if ($(this).parents('.form-item').hasClass("active") == false) {

                $(this).parents('.form-item').addClass("active")
            }
        })
    }
}


//Tooltipster Js 
etGeneric.tooltipFunc = {
    init: function(obj, option) {
        console.log(option)
        $(obj).tooltipster(option)
    }
}


/* Document Ready */
$(function() {

    $("a").each(function() {
        if ($(this).attr('href') == '#' || $(this).attr('href') == ' ') {
            $(this).attr('href', 'javascript:void(0)');
        }
    });


    
    /* Loader Js */
    $(".header-wrapper,.main-wrapper").css('opacity','0');
    $(window).load(function () {
            $(".loader-wrap").hide();
            $(".header-wrapper,.main-wrapper").css('opacity','1');
    });

    if ($(".helper-app-nav-wrap").length != 0) {
        etGeneric.helperAppNav.init();
    }

    /* Home Carousel Slide 1 */ //17-5-16
    if ($(".banner-wrap .owl-carousel").length != 0) {

        var opt = {
            autoplay: true,
            autoplayTimeout: '10000',
            autoplaySpeed: '800',
            smartSpeed: '200',
            autoplayHoverPause: true,
            mouseDrag: false,
            pullDrag: false,
            loop: true,
            items: 1,
            dots: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1
                },
                900: {
                    items: 1
                }
            }
        }

        etGeneric.slideshow.init(".banner-wrap .owl-carousel", opt)
        etGeneric.slideshow.clickEvent(".banner-wrap .owl-carousel");

    }

    /* Home Round-Carousel Slide 3 */
    if ($("#roundCarousel").length != 0) {
        var mobileTest = etGeneric.windowSize.mobile();
        if (mobileTest == false) {
            etGeneric.roundCarousel.init(200);
        } else {
            etGeneric.roundCarousel.init(85);
        }
    }

    /* Home Carousel Slide 2  */
    if ($(".feature-list .owl-carousel").length != 0) {

        var opt = {
            autoplay: true,
            autoplayTimeout: '4000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            mouseDrag: false,
            pullDrag: false,
            loop: true,
            items: 1,
            dots: true,
            nav: false,
            animateIn: "zoomIn",
            animateOut: "fadeOut"
        }

        etGeneric.slideshow.init(".feature-list .owl-carousel", opt)

    }

    /* Home Carousel Slide 2  */
    if ($(".fund-listing .owl-carousel").length != 0) {

        var opt = {
            autoplay: true,
            autoplayTimeout: '4000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            mouseDrag: false,
            pullDrag: false,
            loop: true,
            items: 3,
            dots: true,
            nav: false,
            slideBy: 3,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    slideBy: 1,
                },
                900: {
                    items: 3,
                    slideBy: 3,
                }
            }
        }

        etGeneric.slideshow.init(".fund-listing .owl-carousel", opt)

    }

    /* careers Carousel Slide  */
    if ($(".career-attribute .owl-carousel").length != 0) {

        var opt = {
            autoplay: false,
            autoplayTimeout: '4000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            mouseDrag: false,
            pullDrag: false,
            loop: false,
            items: 1,
            dots: false,
            nav: true,
            autoWidth: false,
            slideBy: 1,
            navContainerClass: 'navigator',
        }

        etGeneric.slideshow.init(".career-attribute .owl-carousel", opt)

    }

    /* Footer Toggle Js */
    if ($(".toggle-footer").length != 0) {
        etGeneric.footerToggle.init();
    }

    // Tab Js:- Blog Listing Page //
    if ($(".tabs").length != 0) {
        etGeneric.customTabs.init(".tabs")


    }


    /* Home Carousel Slide 2  */
    if ($(".legacy-list .owl-carousel").length != 0) {

        var opt = {
            autoplay: true,
            autoplayTimeout: '4000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            mouseDrag: false,
            pullDrag: false,
            loop: true,
            items: 1,
            dots: true,
            nav: false
        }

        etGeneric.slideshow.init(".legacy-list .owl-carousel", opt)

    }

    $(window).load(function() {
        if ($(".team-list").length != 0) {
            var mobileTest = etGeneric.windowSize.mobile();
            if (mobileTest == false) {
                etGeneric.teamDescShow.layout();
            }
            etGeneric.teamDescShow.init();
        }
    })

    /* Home Carousel Slide 2  */
    if ($(".whyUs-listing .owl-carousel").length != 0) {

        var opt = {
            autoplay: false,
            autoplayTimeout: '4000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            mouseDrag: false,
            pullDrag: false,
            loop: true,
            slideBy: 1,
            itemClass: 'ele',
            navContainerClass: 'navigator',
            dots: false,
            nav: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                500: {
                    items: 2,
                    nav: true
                },
                900: {
                    items: 3,
                    nav: true,
                    loop: true
                }
            }
        }

        etGeneric.slideshow.init(".whyUs-listing .owl-carousel", opt)

    }

    /* summary-listing-wrap Carousel Slide 2  */
    if ($(".summary-listing-wrap .owl-carousel").length != 0) {

        var opt = {
            autoplay: false,
            autoplayTimeout: '4000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            mouseDrag: false,
            pullDrag: false,
            loop: true,
            slideBy: 1,
            itemClass: 'ele',
            navContainerClass: 'navigator',
            dots: false,
            nav: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                900: {
                    items: 3,
                    nav: true,
                    loop: true
                }
            }
        }

        etGeneric.slideshow.init(".summary-listing-wrap .owl-carousel", opt)

    }

    /* search  */
    if ($(".search-box").length != 0) {
        etGeneric.searchModule.init();
    }


    if ($(".public-download.circular-carousel-wrap").length != 0) {
        etGeneric.circularCarouselTab.init(".public-download.circular-carousel-wrap")
    }

    if ($(".download-factsheet.circular-carousel-wrap").length != 0) { //25-1-16
        etGeneric.circularCarouselTab.init(".download-factsheet.circular-carousel-wrap")
    }


    if ($(".datePicker").length != 0) {
        etGeneric.customDateTimepicker.cal_Date(".datePicker")

    }
    if ($(".from-to-dp").length != 0) {
        etGeneric.customDateTimepicker.cal_frm_to(".from-to-dp")
    }
    if ($(".timePicker").length != 0) {
        //etGeneric.customDateTimepicker.cal_Date(".timePicker")
    }

    if ($(".yearPicker").length != 0) {
        // etGeneric.customDateTimepicker.cal_Date(".yearPicker")
    }

    if ($(".opening-listing").length != 0) {
        etGeneric.showHide.init(".opening-post-item", ".post-detail-cont", ".know-more-icon")
    }

    // arrow-css
    if ($(".basic-tabs").length != 0 || $(".lang-tabs").length != 0) {
        etGeneric.tabArrow.init();
    }

    // Menu Interaction //
    if ($(".menu-btn").length != 0) {
        etGeneric.menuInteraction.init();
    }

    // Support Interaction //
    if ($(".support-wrap").length != 0) {
        etGeneric.supportInteraction.init();
    }

    // Custome Dropdown 
    if ($(".select-box").length != 0) {
        var opt = {
            "dropdownAutoWidth": true,
            "minimumResultsForSearch": -1,
            "containerCssClass": "select-box",
            "dropdownCssClass": "select-box-list",
            "dropdownCss": "test1"
        }
        etGeneric.customeSelect.init(".select-box", opt);
    }

    if ($(".basic-tabs .tab-with-line").length != 0) {
        var dataArray = [];
        $(".basic-tabs .tab-with-line .tab-link-wrap").each(function(d) {
            var obj = $(this);
            var newArray = [];
            $(obj).find("li").each(function(e) {
                //$(this).parents(".tab-link-wrap").hide();
                var data = $(this).children("a").text();
                var dtobj = {};
                dtobj.id = e;
                dtobj.text = data;
                newArray.push(dtobj)

            });
            dataArray.push(newArray);
            $(obj).parent().prepend('<input class="tab-dropdown" type="hidden" id="tab-line' + d + '"/>')
            $('#tab-line' + d).select2({
                data: {
                    results: newArray
                },
                "dropdownAutoWidth": true,
                "minimumResultsForSearch": -1,
                "containerCssClass": "select-box dark-theme",
                "dropdownCssClass": "select-box-list",
                initSelection: function(element, callback) {
                    var data = newArray[0];
                    callback(data);
                }
            }).on("change", function(e) {
                $(obj).find(".tab-link").eq(e.val).click();
                //console.log($(obj).find(".tab-link").eq(0).html())
            });


        });
        //console.log(dataArray);
    }

    if ($(".quick-item-cont").length != 0) {
        etGeneric.mobile_FooterToogle.init();
    }


    $(window).resize(function() {
        if ($("#roundCarousel").length != 0) {
            var mobileTest = etGeneric.windowSize.mobile();
            if (mobileTest == false) {
                etGeneric.roundCarousel.init(200);
            } else {
                etGeneric.roundCarousel.init(85);
            }
        }
    });

    // Custome Dropdown 
    if ($(".search-select-box").length != 0) {
        var opt = {
            "dropdownAutoWidth": true,
            "containerCssClass": "select-box",
            "dropdownCssClass": "select-box-list",
            "dropdownCss": "test1"
        }
        etGeneric.customeSelect.init(".search-select-box", opt);
    }

    $('#schedule-link').click(function() {
        if ($(this).prop("checked") == true) {
            $(".schedule-time").find(".select-box").removeAttr('disabled');
        } else if ($(this).prop("checked") == false) {
            $(".schedule-time").find(".select-box").attr('disabled', 'disabled');
        }
    });

    if ($(this).find(".stories-wrapper").length != 0) {

        var opt = {
            itemSelector: '.story-list-item',

        }
        etGeneric.customMasonry.init(".stories-wrapper ul", opt);
    }

    if ($(".buzz-listing").length != 0) {
        var mobileTest = etGeneric.windowSize.mobile();
        if (mobileTest == true) {
            etGeneric.customAccordian.init(".buzz-listing", ".sec-title", ".buzz-cont", "0", "open")
        }
    }

    if ($(".product-list.equalizer-height").length != 0) {
        etGeneric.equalizerHeight.init(".product-list.equalizer-height", ".equalizer-item");

    }

    if ($(".equalizer-height.feature-row").length != 0) {
        etGeneric.equalizerHeight.init(".equalizer-height.feature-row", ".equalizer-item", "3");

    }



    if ($(".screen-overlay").length != 0) {
        etGeneric.screenOverlay.init();
    }

    if ($(this).find(".masonry-list").length != 0) {
        var opt = {
            itemSelector: '.social-connect-item',
            transitionDuration: '0.4s'
        }
        etGeneric.customMasonry.init(".masonry-list", opt);
    }

    if ($(".flyout-item-container").length != 0) {
        var opt = {
            axis: "x",
            autoHideScrollbar: false,
            alwaysShowScrollbar: true,
            setLeft: 0,
            mouseWheel: {
                enable: true
            },
            advanced: {
                updateOnContentResize: true
            }

        }
        etGeneric.customScrollbar.init(".flyout-item-container", opt)
    }

    // tooltip js boostrap
    if ($("[data-toggle='tooltip']").length != 0) {
        $('[data-toggle="tooltip"]').tooltip().data('bs.tooltip').tip().addClass('basic-tooltip');
    }


    $(".panel-group .accordion-head").on('click', function() {
        $(this).parents(".panel-group").find(".panel").removeClass('active')
        $(this).parent(".panel").addClass('active');
    });

    if ($(".fund-slider").length != 0) {
        var opt = {
            start: [400000],
            range: {

                'min': 100000,
                'max': 1000000
            },
            step: 100000,
            tooltips: wNumb({
                decimals: 0,
                thousand: ',',
                prefix: 'Rs '
            }),
            evupdate: function(obj) {
                slideValue = obj.noUiSlider.get();

                var newData = {
                    labels: ['1 year', '3 year', 'since inception'],
                    series: [{
                        "name": "Fund Performance",
                        "data": [5, 4, 3]

                    }, {

                        "name": "Benchmark",
                        "data": [6, 7, 8]
                    }]
                }

                //console.log( $(".tab-with-line .tab-link").length)
                var indx = $(".tab-with-line .tab-link.active").index();
                //console.log("ind" + indx)
                //console.log($(".tab-content").eq(indx).find(".fund_bargraph").attr("data-chart-name"))
                var ch = chrtObj[$(".tab-content").eq(indx).find(".fund_bargraph").attr("data-chart-name")]
                ch.update(newData);

            }
        }
        etGeneric.valueSlider.init(".fund-slider", opt)
    }

    if ($(".fund-demo").length != 0) {
        var opt = {
            start: [1],
            range: {

                'min': 1,
                'max': 10
            },
            step: 1,
            evupdate: function(obj) {
                slideValue = obj.noUiSlider.get();
            }
        }
        etGeneric.valueSlider.init(".fund-demo", opt)
    }

    // slide-result-wrap
    if ($(".slider1").length != 0) {
        var opt = {
            start: [30],
            range: {

                'min': 18,
                'max': 70
            },
            step: 1,
            tooltips: wNumb({
                decimals: 0
            }),
            evslide: function(obj) {
                slideValue = obj.noUiSlider.get();
                console.log($(obj).remove())
            }
        }

        etGeneric.valueSlider.init(".slider1", opt)

    }
    // sleek-value-slider
    if ($(".demo-js-slider").length != 0) {
        var opt = {
            start: [30],
            range: {

                'min': 18,
                'max': 70
            },
            step: 1,
            tooltips: wNumb({
                decimals: 0
            })
        }
        etGeneric.valueSlider.init(".demo-js-slider", opt);
    }

    $('.click-selection input[type=checkbox]').click(function() {
        if ($(this).prop("checked") == true) {
            $(".click-selection").find(".click-open-box").show(1);
        } else if ($(this).prop("checked") == false) {
            $(".click-selection").find(".click-open-box").hide(1);
        }
    });


    if ($(".quick-view-listing").length != 0) {

        var arrangeList = [
            [0, 8],
            [1, 2, 3, 4],
            [5, 6, 7]
        ];

        etGeneric.quickView.layout(".quick-view-listing", arrangeList, "col-sm-8");

    }

    // need analyzer profile change js 
    if ($(".need-analyzer .profile-sec").length != 0) {
        $(".need-analyzer .profile-sec").on('click', function() {
            $(this).toggleClass("active");
            $(this).parents(".need-analyzer").find(".user-initial-form").slideToggle(500);
        })
    }

    // Need Analyzer panel js
    // if ($(".panel-js-toggle").length != 0) {
    //     $(".panel-js-toggle").on('click', function() {
    //         $(this).toggleClass("active");
    //         $(".sections.panel-close").slideToggle(500);
    //     });
    // }

     if ($(".panel-js-toggle").length != 0) {
        console.log($(".need-analyzer .sections").length)
        $(".need-analyzer .panel-js-toggle").on('click', function() {
            var ind = $(".need-analyzer .sections").index($(this).parents(".sections"));
            console.log(ind);
            $(this).toggleClass("active");
            $(".sections").eq(ind-1).slideToggle(500);
        });
    }


    // Account Add Member Profile //
    if ($(".myaccount-wrap .plus-icon-wrap").length != 0) {
        etGeneric.addnewStructBefore.init(".myaccount-wrap .plus-icon-wrap", ".repeat-obj")
    }

    // Account Application Js //
    if ($(".track-application-wrap .step-process-list").length != 0) {
        etGeneric.trackAppContenToggle.init();
    }

    // Account Sub Nav Toggle //
    if ($(".estatements-toggle").length != 0) {

        etGeneric.accountsubNavToggle.init(".estatements-toggle", ".estatements-link")
    }

    if ($(".etransaction-toggle").length != 0) {
        etGeneric.accountsubNavToggle.init(".etransaction-toggle", ".etransactions-link")
    }

    if ($("#testForm").length != 0) {
        $("#testForm").validate();
    }

    $('#login').on('shown.bs.modal', function() {
        if ($(".basic-tabs").length != 0 || $(".lang-tabs").length != 0) {
            etGeneric.tabArrow.init();
        }
    });

    if ($("#uploadBtn").length != 0) {
        $("#uploadBtn").on('change', function() {

            $("#uploadFile").val($("#uploadBtn").val());
        })
    }

    if ($(".msg-btn").length != 0) {
        etGeneric.intelligenceMsg.init();
    }

    if ($(".equalizer-height.data-listing").length != 0) {
        $(".equalizer-height.data-listing").css('opacity', '0');
    }
    $('.modal').on('shown.bs.modal', function() {

        if ($(".equalizer-height.data-listing").length != 0) {



            etGeneric.equalizerHeight.init(".equalizer-height.data-listing", ".equalizer-item", "3");
            setTimeout(function() {
                $(".equalizer-height.data-listing").css('opacity', '1');
            }, 200)

        }

    });


     //  Helper Screen Position Js // 21-3-16
    if ($(".hs-home").length != 0) {
        var arrayObj = [
            [".yourNeed", ".calculator", ".freeQuote"],
            [".sign-up"],
            [".reveal-link-wrap .character"]
        ]
        etGeneric.helpTutorialPosFunc.init(arrayObj);

        if ($(".hs-home-wrap ").hasClass("active") == true) {
            //alert("init")
            $(".banner-wrap .owl-carousel").trigger('stop.owl.autoplay')
        }
    }

    $(".needanalyzer-nextStep").on('click', function(e) {

        e.preventDefault();
    });


    //mobile blog menu js
    if ($(".mob-item-listing").length != 0) {
        $(".page-back").on('click', function() {
            $(this).toggleClass("active");
            $(".mob-item-listing").slideToggle();
        });
    }

    //check uncheck js : need analyzer
    if ((".need-analyzer .spouse-icon").length != 0) {
        etGeneric.chckUnchckFunc.init(".need-analyzer .radiobox-wrap input[type=radio]", ".need-analyzer .spouse-icon", "yes-btn", "no-btn")
    }

    // add remove class js : your profile
    if ((".your-profile-wrap input[type=text]").length != 0) {
        etGeneric.addRemoveClassFunc.init();
    }

    var windowWidth = $(window).width();
    var mobileTest = etGeneric.windowSize.mobile();


    if (mobileTest == false) {
        // Product Briefcase - More option click Tooltipster : desktop
        if ($(".more-option-warp").length != 0) {
            var opt = {
                functionInit: function() {
                    return $(this).find(".moreoption-content").html()
                },
                theme: 'theme5',
                contentAsHTML: true,
                positionTracker: true,
                onlyOne: true,
                position: 'right',
                minWidth: 320,
                arrow: false,
                trigger: 'click',
                offsetX: -35,
                autoClose: true,
                functionReady: function() {
                    $('.more-option-warp').css({ 'z-index': 0 })
                    $(this).find('.icon').addClass("active");
                    $(this).css({
                        'z-index': 2
                    })
                },
                functionAfter: function() {
                    $('.more-option-warp').css({ 'z-index': 0 });
                    $('.more-option-warp').find('.icon').removeClass("active");
                }
            }
            etGeneric.tooltipFunc.init(".more-option-warp", opt)
        }

    }

    if (mobileTest == true) {

        // Product Briefcase - More option click Tooltipster : Mobile
        if ($(".more-option-warp").length != 0) {
            var opt = {
                functionInit: function() {
                    return $(this).find(".moreoption-content").html()
                },
                theme: 'theme5',
                contentAsHTML: true,
                positionTracker: true,
                onlyOne: true,
                position: 'right',
                maxWidth: 125,
                arrow: false,
                trigger: 'click',
                offsetX: -35,
                offsetY: -100,
                autoClose: true,
                functionReady: function() {
                    $('.more-option-warp').css({ 'z-index': 0 })
                    $(this).css({
                        'z-index': 2
                    })
                },
                functionAfter: function() {
                    $('.more-option-warp').css({ 'z-index': 0 });
                    $('.more-option-warp').find('.icon').removeClass("active");
                }
            }
            etGeneric.tooltipFunc.init(".more-option-warp", opt)
        }

        //Quick View Page hover tooltipster
        if ($(".quick-view-item .hover-tooltip-1").length != 0) {
            var opt = {
                theme: 'theme2',
                position: 'top-right',
                trigger: 'hover',
                arrow: false,
                offsetY: -67,
                offsetX: 0,
                contentAsHTML: true,
                minWidth: 250
            }
            etGeneric.tooltipFunc.init(".quick-view-item .hover-tooltip-1", opt)
        }

    }

    //Your Profile,Product Briefcase,Need Summary hover Tooltipster
    if ($(".hover-tooltip-1").length != 0) {
        var opt = {
            theme: 'theme2',
            position: 'top-right',
            arrow: false,
            offsetY: -67,
            offsetX: 0,
            contentAsHTML: true,
            minWidth: 270
        }
        etGeneric.tooltipFunc.init(".hover-tooltip-1", opt)
    }

    //Product detail - icon hover toolipster
    if ($(".hover-tooltip-2").length != 0) {
        var opt = {
                theme: 'theme4',
                position: 'bottom',
                trigger: 'hover'
            }
            // alert("jhjhj")
        etGeneric.tooltipFunc.init(".hover-tooltip-2", opt)
    }

    // Product Briefcase - Graph Image hover Tooltipster 
    if ($(".icon-link-tooltip .icon").length != 0) {
        var opt = {
            theme: 'theme1',
            maxWidth: 290,
            content: $('<span class=graphTooltip><img src="images/dummy_graph.jpg"/> </span>')
        }
        etGeneric.tooltipFunc.init(".icon-link-tooltip .icon", opt)
    }


    // Need Analyzer - Profile Image Tooltipster
    if ($(".profile-img").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parent().find(".profile-tooltip").html()
            },
            contentAsHTML: true,
            positionTracker: true,
            onlyOne: true,
            position: 'left',
            minWidth: 100,
            functionReady: function() {
                $('.tooltipster-arrow').removeClass('tooltipster-arrow-top-left').addClass('tooltipster-arrow-left');
            },
            theme: 'theme3'
        }
        etGeneric.tooltipFunc.init(".profile-img", opt)
    }



    // Demo Portfolio - Indicator Image hover Tooltipster
    if ($(".indicator").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).find(".indicator-content").html()
            },
            theme: 'theme1',
            contentAsHTML: true,
            positionTracker: true,
            position: 'top-right',
            maxWidth: 195,
            minWidth: 195,
            arrow: false,
            onlyOne: true
        }
        etGeneric.tooltipFunc.init(".indicator", opt)
    }

    // My Acoounts Pages - Notification Click Tooltipster
    if ($(".notifyLink").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parents(".notification-wrap").find(".notifylist-content").html()
            },
            theme: 'theme1',
            maxWidth: 290,
            trigger: 'click',
            contentAsHTML: true,
            arrow: false,
            positionTracker: true,
            onlyOne: true,
            position: 'bottom-right',
            offsetY: 5,
            offsetX: 30,
            minWidth: 290
        }
        etGeneric.tooltipFunc.init(".notifyLink", opt)
    }

    // Tract Products Page  - Policy Detail Click Tooltipster
    if ($(".policy-link").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parent().find(".policyDetail-content").html()
            },
            theme: 'theme1',
            position: 'bottom-left',
            trigger: 'click',
            contentAsHTML: true,
            arrow: false,
            positionTracker: true,
            onlyOne: true,
            maxWidth: 300,
            minWidth: 270
        }
        etGeneric.tooltipFunc.init(".policy-link", opt)
    }

    // Main Template,Buying Journey Page   - edit btn Click Tooltipster
    if ($(".edit-btn").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parent().find(".selectRate-content").html()
            },
            theme: 'theme1',
            position: 'bottom-left',
            trigger: 'click',
            contentAsHTML: true,
            arrow: false,
            positionTracker: true,
            onlyOne: true,
            maxWidth: 106,
            minWidth: 106,
            functionReady: function() {
                $(this).text('close'),
                    $(".select-rate").on('click', function() {
                        $(this).addClass("active");
                        $(".info-amount-val").text($(this).text())
                    })
            },
            functionAfter: function() {
                $(this).text('edit')
            }
        }
        etGeneric.tooltipFunc.init(".edit-btn", opt)
    }



    // Re-applocation form Page  - hover Tooltipster
    if ($(".re_application_form .tooltip-cont-wrap").length != 0) {
        var opt = {
            theme: 'theme2',
            position: 'bottom-left',
            contentAsHTML: true,
            positionTracker: true,
            onlyOne: true,
            maxWidth: 240,
            minWidth: 240
        }
        etGeneric.tooltipFunc.init(".re_application_form .tooltip-cont-wrap .icon", opt)
    }

    // Your Profile Page   - Profile Image Edit Click Tooltipster
    if ($(".editTooltip").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parent().find(".editTooltip-content").html()
            },
            theme: 'theme1',
            position: 'bottom-left',
            trigger: 'click',
            contentAsHTML: true,
            arrow: false,
            positionTracker: true,
            onlyOne: true
        }
        etGeneric.tooltipFunc.init(".editTooltip", opt)
    }


    // Need Analyzer Summary Page   - view parameters Click Tooltipster

    if ($(".click-info").length != 0) {
        var opt = {
            functionInit: function() {
                return $(this).parent().find(".viewinfo-content").html()
            },
            theme: 'theme1',
            position: 'bottom-left',
            trigger: 'click',
            contentAsHTML: true,
            positionTracker: true,
            onlyOne: true,
            maxWidth: 240,
            minWidth: 240
        }
        etGeneric.tooltipFunc.init(".click-info", opt)
    }

    /* summary-listing-wrap Carousel Slide 2  */
    if ($(".detail_banner .owl-carousel").length != 0) {

        var opt = {
            autoplay: true,
            autoplayTimeout: '10000',
            autoplaySpeed: '800',
            smartSpeed: '400',
            autoplayHoverPause: true,
            loop: true,
            margin: 10,
            nav: false,
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            },
            item: 1
        }

        etGeneric.slideshow.init(".detail_banner .owl-carousel", opt)

    }


        $(".plus-minus .plusMinus-btn").on('click', function() {
        var obj = this;
        var plusVal = parseInt($(obj).parent(".plus-minus").attr('data-plus-limit'));
        var minVal = parseInt($(obj).parent(".plus-minus").attr('data-minus-limit'));
        if ($(obj).hasClass("right-btn") == true) {
            if (parseInt($(obj).parent(".plus-minus").find(".value").text()) < plusVal || $(obj).parent(".plus-minus").attr('data-plus-limit') == undefined) {
                
                var tempVal = parseInt($(obj).parent(".plus-minus").find(".value").text());
                tempVal++;
                console.log("plus" +tempVal)
                $(obj).parent(".plus-minus").find(".value").html(tempVal)
            }
        } else {

            if (parseInt($(obj).parent(".plus-minus").find(".value").text()) > minVal || $(obj).parent(".plus-minus").attr('data-minus-limit') == undefined) {
                var tempVal = parseInt($(obj).parent(".plus-minus").find(".value").text());
                tempVal--;
                console.log("min" + tempVal)
                $(obj).parent(".plus-minus").find(".value").html(tempVal)
            }
        }


    });



    //  var mobileTest = etGeneric.windowSize.mobile();


    // if (mobileTest == false) {

    //     $(".detail_banner .owl-item").each(function(){

    //         var imgSrc = $(this).find('img').attr('src').split(".");
    //         var finalSrc = imgSrc[0]+"_mobile."+imgSrc[1];
    //         console.log(finalSrc);


    //     });

    // }



    $(".anchor-bg").each(function(){

     var imgSrc = $(this).find('img').attr('src');
     $(this).css('background-image','url('+imgSrc+')');

    });

    // my-account js start 21-3-16 new added
     var mobileTest = etGeneric.windowSize.mobile();
    if (mobileTest == true) {
    $(".my-account").on('click', function() {
        $(".account-wrap").addClass("active").css({"left": "0"});
    });
    $(".account-wrap .icon-close").on('click', function() {
        $(".account-wrap").removeClass("active").css({"left": "100%"});
    });
}

   
    
// my-account js end

});
