/*
  @Author : Fractalink Design Studio
  @Project : Edelwesis Website
  @Dev : Irfan Khan
  @Date : 26th Nov 2015;

*/
var etTimeline = {}

etTimeline.animateStr = {
    timer: "",
    timer1: "",
    selectedVal: {
        gender: {
            genderselected: ''
        },
        birthdate: {
            dob: '',
            age: ''
        },
        dependent: {
            spouse: false,
            childrens: '',
            parents: ''
        },
        needs: {
            securedfuture: false,
            healthyfuture: false,
            plannedfuture: {
                educationplan: false,
                growthplan: false,
                retirementplan: false
            }
        }
    },
    init: function() {
        etTimeline.animateStr.floatingAni();
        etTimeline.animateStr.mouseIntraction();
        // $('.spouse-box #spouse').click(function() { remove-17-5-16
        //     if ($(this).prop("checked") == true) {
        //         etTimeline.animateStr.selectedVal.dependent.spouse = true;
        //     }
        //     if ($(this).prop("checked") == false) {
        //         etTimeline.animateStr.selectedVal.dependent.spouse = false;
        //     }

        // });

        // 17-5-16  add js
        $(".spouse-box .icon-unselect").on('click', function() {
            $(this).parents(".spouse-box").removeClass("active");
            etTimeline.animateStr.selectedVal.dependent.spouse = false;
        });
        $(".spouse-box .icon-select").on('click', function() {
            $(this).parents(".spouse-box").addClass("active");
            etTimeline.animateStr.selectedVal.dependent.spouse = true;
        });

        $(".children-box  .left-btn").on('click', function() {

            if ($(".children-box").find(".value").text() > 0) {

                var tempVal = parseInt($(".children-box").find(".value").text());
                etTimeline.animateStr.selectedVal.dependent.childrens = tempVal;

                etTimeline.animateStr.selectedVal.needs.plannedfuture.educationplan = true;
                $(".education-panning-box,.planned-future-box").show();

            }

            if ($(".children-box").find(".value").text() == 0) {

                var tempVal = parseInt($(".children-box").find(".value").text());
                etTimeline.animateStr.selectedVal.dependent.childrens = tempVal;

                etTimeline.animateStr.selectedVal.needs.plannedfuture.educationplan = false;
                $(".education-panning-box").hide();
            }


        });

        $(".children-box .right-btn").on('click', function() {

            if ($(".children-box").find(".value").text() < 10) {

                var tempVal = parseInt($(".children-box").find(".value").text());
                etTimeline.animateStr.selectedVal.dependent.childrens = tempVal;
                etTimeline.animateStr.selectedVal.needs.plannedfuture.educationplan = true;
                $(".education-panning-box,.planned-future-box").show();
            }

        });

        $(".parents-box .left-btn").on('click', function() {
            if ($(".parents-box").find(".value").text() > 0) {
                var tempVal = parseInt($(".parents-box").find(".value").text());

                etTimeline.animateStr.selectedVal.dependent.parents = tempVal;

            }
        });

        $(".parents-box .right-btn").on('click', function() {
            var tempVal = parseInt($(".parents-box").find(".value").text());
            etTimeline.animateStr.selectedVal.dependent.parents = tempVal;
        });

        $(".gender").on("click", function() {
            $(".gender").removeClass("gender-selected").removeClass("floating-ani").addClass("hidden")
            $(this).removeClass('hidden').addClass("gender-selected");
            etTimeline.animateStr.stepActive(1);
            etTimeline.animateStr.selectedVal.gender.genderselected = $(".gender-selected").attr('data-gender');


        });



        $(".need-selection .cloud-wrap input:checkbox").change(function() {
            if ($(this).parents().hasClass("secured-future-box")) {
                if ($(this).prop("checked") == true) {
                    etTimeline.animateStr.selectedVal.needs.securedfuture = true;
                } else if ($(this).prop("checked") == false) {
                    etTimeline.animateStr.selectedVal.needs.securedfuture = false;
                }
                //alert("securedfuture")
            } else if ($(this).parents().hasClass("healthy-future-box")) {
                if ($(this).prop("checked") == true) {
                    etTimeline.animateStr.selectedVal.needs.healthyfuture = true;
                } else if ($(this).prop("checked") == false) {
                    etTimeline.animateStr.selectedVal.needs.healthyfuture = false;
                }
                //alert("healthyfuture")
            } else if ($(this).parents().hasClass("education-panning-box")) {
                if ($(this).prop("checked") == true) {
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.educationplan = true;
                } else if ($(this).prop("checked") == false) {
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.educationplan = false;
                }
                //alert("educationplan")
            } else if ($(this).parents().hasClass("growth-panning-box")) {
                if ($(this).prop("checked") == true) {
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.growthplan = true;
                } else if ($(this).prop("checked") == false) {
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.growthplan = false;
                }
                //alert("growthplan")
            } else if ($(this).parents().hasClass("retirement-panning-box")) {
                if ($(this).prop("checked") == true) {
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.retirementplan = true;
                } else if ($(this).prop("checked") == false) {
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.retirementplan = false;
                }
                //alert("retirementplan")
            }

            //console.log(etTimeline.animateStr.selectedVal)

        });




    },
    floatingAni: function() {
        $(".gender-selection .gender-male").addClass("floating-ani");
        clearTimeout(etTimeline.animateStr.timer);
        etTimeline.animateStr.timer = setTimeout(function() {
            $(".gender-selection .gender-female").addClass("floating-ani");
        }, 2000);

    },
    mouseIntraction: function() {
        $(".gender-selection .gender").hover(function() {
            if ($(this).hasClass(".gender-selected") == false) {
                clearTimeout(etTimeline.animateStr.timer);
                $(".gender").removeClass("floating-ani").removeClass("mouse-over").addClass("mouse-out")
                $(this).removeClass("mouse-out").addClass("mouse-over")
            }

        })
        $(".gender-selection").on('mouseout', function() {
            if ($(this).find(".gender-selected").length != 1) {
                etTimeline.animateStr.floatingAni();
                $(".gender").removeClass("mouse-out").removeClass("mouse-over")
            }
        })


    },
    stepActive: function(objIndx) {

        $(".timeline-section").removeClass("active");
        $(".timeline-section").find(".scale-up-ani").removeClass("scale-up-ani");
        $(".timeline-section").find(".slide-down-ani").removeClass("slide-down-ani");
        $(".timeline-section").eq(objIndx).addClass("active");
        etTimeline.animateStr.updateNav(objIndx);
        var aniEleArr = $(".timeline-section").find("[data-ani-type]")

        $(aniEleArr).css("opacity", "0")
        var cont = 0;

        var tt = setInterval(function() {
            if (cont < aniEleArr.length) {
                $("[data-ani-seq=" + cont + "]").addClass($("[data-ani-seq=" + cont + "]").attr("data-ani-type"))
                cont++
            } else {
                clearInterval(tt);
            }

        }, 200);

    },

    nextClick: function() {
        $(".timeline-section .next-btn").on('click', function() {
            var indx = $(this).index(".timeline-section .next-btn");
            etTimeline.animateStr.stepActive(indx + 2);
            etTimeline.animateStr.updateNav(indx + 2);

            if ($(this).parents().hasClass("timeline-section4")) {
                console.log("all data capture")
                console.log(etTimeline.animateStr.selectedVal);
            }
        });

    },
    navClick: function() {
        $(".timeline-steps-wrap li").on('click', function() {
            if ($(this).hasClass("done") || $(this).hasClass("processing")) {
                var indx = $(this).index();
                etTimeline.animateStr.stepActive(indx);
                etTimeline.animateStr.updateNav(indx);

            }

        });

    },
    updateNav: function(selectIndx) {
        $(".timeline-steps-wrap li").removeClass("processing done");
        $(".timeline-steps-wrap li").eq(selectIndx).addClass("processing");
        $(".timeline-steps-wrap li").eq(selectIndx).prevAll().addClass("done");
        if (selectIndx == 0) {
            $(".gender").removeClass("gender-selected hidden");

        }
    }
}

etTimeline.helpTooptip = {
    init: function() {
        $(".cloud-wrap .help-icon").on('click', function() {

            if ($(window).width() > 768) {
                var leftPos = $(this).offset().left;
                var topPos = $(this).offset().top;
            }

            $("body .cloud-tooltip").remove();
            $("body").append("<div class='cloud-tooltip'></div>");
            var currData = $(this).parent().find(".cloud-info-toolTip");
            $(".cloud-tooltip").append($(currData).clone());

            $(".cloud-tooltip").css({
                left: leftPos,
                top: topPos,
                position: 'absolute'
            })
        });

        $("body").on('click', ".close-icon", function() {
            $(".cloud-tooltip").remove();
        });

        $("body").click(function(event) {

            if ($(event.target).parents(".cloud-tooltip").length == 0 && $(event.target).hasClass("help-icon") == false) {

                $(".cloud-tooltip").remove();

            }

        });
    }
}



$(function() {

    //$('.timeline-wrap #spouse').removeAttr('checked'); remove-17-5-16

    $(".timeline-wrap .spouse-icon").removeClass("active");   // 17-5-16  add js

    etTimeline.animateStr.selectedVal.dependent.spouse = false;


    $(".cloud-wrap").hide();

    $(".need-selection input:checkbox").each(function() {
        $(this).prop("checked", true);
    });

    etTimeline.animateStr.init();
    etTimeline.animateStr.stepActive(0);
    etTimeline.animateStr.nextClick();
    etTimeline.animateStr.navClick();
    etTimeline.helpTooptip.init();

    if ($(".timeline-age-cal").length != 0) {

        var opt = {
            start: [30],
            range: {

                'min': 18,
                'max': 70
            },
            step: 1,
            tooltips: wNumb({
                decimals: 0
            }),
            format: wNumb({
                decimals: 0,

            }),
            evupdate: function(obj) {

                slideValue = obj.noUiSlider.get();
                $(".ageWidget-cont").show();
                dobYear = currentYear - slideValue;

                function showNextBtn() {
                    $(".next-btn").show();
                    //alert($(".date-show").html())
                    etTimeline.animateStr.selectedVal.birthdate.dob = $(".date-show").html();
                    //console.log(etTimeline.animateStr.selectedVal)
                }
                ageWidget.skeleton.monthTable(currentMonth, dobYear - 1, $(obj).parent(), showNextBtn);
                // console.log("init" )
                etTimeline.animateStr.selectedVal.needs.plannedfuture.growthplan = false;
                etTimeline.animateStr.selectedVal.needs.securedfuture = false;
                etTimeline.animateStr.selectedVal.needs.healthyfuture = false;
                etTimeline.animateStr.selectedVal.needs.plannedfuture.retirementplan = false;
                $(".secured-future-box,.healthy-future-box,.planned-future-box,.retirement-panning-box,.growth-panning-box").hide();

                if (obj.noUiSlider.get() > 18 && obj.noUiSlider.get() < 60) {
                    $(".secured-future-box").show();
                    //console.log("securedfuture")
                    etTimeline.animateStr.selectedVal.needs.securedfuture = true;
                }


                if (obj.noUiSlider.get() > 18 && obj.noUiSlider.get() < 65) {
                    $(".healthy-future-box").show();
                    //console.log("healthyfuture")
                    etTimeline.animateStr.selectedVal.needs.healthyfuture = true;
                }


                if (obj.noUiSlider.get() > 40) {
                    $(".planned-future-box,.retirement-panning-box").show();
                    //console.log("retirementplan")
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.retirementplan = true;
                }


                if (obj.noUiSlider.get() > 22 && obj.noUiSlider.get() < 50) {

                    $(".planned-future-box,.growth-panning-box").show();
                    //console.log("growthplan")
                    etTimeline.animateStr.selectedVal.needs.plannedfuture.growthplan = true;

                }



                //console.log(etTimeline.animateStr.selectedVal)
            },
            evslide: function(obj) {

                $(".ageWidget-cont .monthTable").remove();
                if ($(".age-selection").length != 0) {
                    $(".age-selection .value").text(obj.noUiSlider.get());
                    etTimeline.animateStr.selectedVal.birthdate.age = obj.noUiSlider.get();
                    //console.log(etTimeline.animateStr.selectedVal)
                    if ($(window).width() > 768) {
                        if (obj.noUiSlider.get() > 36 && obj.noUiSlider.get() < 45) {
                            if ($(".gender-selected.gender").hasClass("gender-male")) {
                                $(".gender-male").css('background-position', '0px -370px');
                            } else {
                                $(".gender-female").css('background-position', '-272px -370px')
                            }
                        } else if (obj.noUiSlider.get() > 0 && obj.noUiSlider.get() < 35) {

                            if ($(".gender-selected.gender").hasClass("gender-male")) {
                                $(".gender-male").css('background-position', '0px 0px');

                            } else {
                                $(".gender-female").css('background-position', '-272px 0px')
                            }
                        } else if (obj.noUiSlider.get() > 46 && obj.noUiSlider.get() < 60) {

                            if ($(".gender-selected.gender").hasClass("gender-male")) {

                                $(".gender-male").css('background-position', '0px -750px');
                            } else {
                                $(".gender-female").css('background-position', '-272px -750px')
                            }
                        } else if (obj.noUiSlider.get() > 61 && obj.noUiSlider.get() < 70) {

                            if ($(".gender-selected.gender").hasClass("gender-male")) {

                                $(".gender-male").css('background-position', '0 -1131px');
                            } else {
                                $(".gender-female").css('background-position', '-272px -1131px')
                            }
                        }

                    } else {
                        if (obj.noUiSlider.get() > 36 && obj.noUiSlider.get() < 45) {
                            if ($(".gender-selected.gender").hasClass("gender-male")) {

                                $(".gender-male").css('background-position', '-563px -570px');
                            } else {
                                $(".gender-female").css('background-position', '-838px -570px')
                            }
                        } else if (obj.noUiSlider.get() > 0 && obj.noUiSlider.get() < 35) {

                            if ($(".gender-selected.gender").hasClass("gender-male")) {
                                $(".gender-male").css('background-position', '-563px -190px');

                            } else {

                                $(".gender-female").css('background-position', '-838px -190px')
                            }
                        } else if (obj.noUiSlider.get() > 46 && obj.noUiSlider.get() < 60) {

                            if ($(".gender-selected.gender").hasClass("gender-male")) {

                                $(".gender-male").css('background-position', '-563px -950px');
                            } else {
                                $(".gender-female").css('background-position', '-838px -950px')
                            }
                        } else if (obj.noUiSlider.get() > 61 && obj.noUiSlider.get() < 70) {

                            if ($(".gender-selected.gender").hasClass("gender-male")) {

                                $(".gender-male").css('background-position', '-563px -1330px');
                            } else {
                                $(".gender-female").css('background-position', '-838px -1330px')
                            }
                        }
                    }



                }
            }
        }


        ageWidget.create(".timeline-age-cal", opt);



    }


});
